/**
 * @format
 */

import { AppRegistry } from 'react-native';
// import App from './App';
import Root from './src/root';
// import Router from './src/router';
import Login from './src/screens/login/Login';
import Home from './src/screens/home/Home';
import Absen from './src/screens/absen/Absen'
import { name as appName } from './app.json';

AppRegistry.registerComponent(appName, () => Root);
