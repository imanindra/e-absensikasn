export default {
    //image Custome
    bg_custome: require('./bg_custome.png'),
    // Icon Logo
    icon_logo: require('./ic_logo.png'),
    // Icon Verify
    icon_verify: require('./ic_verifiy.png'),
    icon_noverify: require('./ic_noverify.png'),
    // Icon Logout
    icon_logout: require('./ic_logout.png'),
    // Icon Menu Home
    icon_absent: require('./ic_absent.png'),
    icon_profile: require('./ic_profile.png'),
    icon_TM: require('./ic_time_management.png'),

    // Icon Back
    icon_back: require('./ic_back.png'),

    // Absent
    icon_lokasi: require('./ic_lokasi.png'),
    icon_masuk: require('./ic_scanmasuk.png'),
    icon_masuk_disable : require('./ic_in_disable.png'),
    icon_plg_disable : require('./ic_plg_disable.png'),
    icon_pulang: require('./ic_scanpulang.png'),
    icon_logabsen: require('./ic_logtoday.png'),

    icon_note: require('./ic_note.png'),

    icon_checklist: require('./ic_checklist.png'),
    icon_warning: require('./ic_warning.png'),
    icon_cross: require('./ic_cross.png'),
    icon_camera: require('./ic_camera.png'),

    // REPORT
    icon_day: require('./ic_day.png'),
    icon_month: require('./ic_month.png'),

    // ICON ROUNDER
    icon_rounde_active: require('./ic_round.png'),
    icon_rounde_disable: require('./ic_rounde_disable.png'),

    // ICON TAB PEGAWAI
    icon_pg_active : require('./ic_pwg_active.png'),
    icon_pg_disable : require('./ic_pwg_disable.png'),
    icon_uk_active : require('./ic_uk_active.png'),
    icon_uk_disable : require('./ic_uk_disable.png'),
    icon_ak_active : require('./ic_tk_active.png'),
    icon_ak_disable : require('./ic_tk_disable.png'),
    icon_pwd_active : require('./ic_pwd_active.png'),
    icon_pwd_disable : require('./ic_pwd_disable.png'),
}   
