import React from 'react'
import { View, Text, StyleSheet, Dimensions, Image, TouchableOpacity } from 'react-native'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import assets from '../assets'
import color from '../constant/Color'
import Font from '../constant/Font'
const { width, height } = Dimensions.get('window');


const styles = StyleSheet.create({
    container: {
        height: height * 0.065,
        // backgroundColor: color.COLOR.YELLOW,
        position: 'absolute',
        alignItems: 'center',
        justifyContent: 'center',
        // shadowColor: "#000000",
        // shadowOpacity: 0.8,
        // shadowRadius: 2,
        // shadowOffset: {
        // height: 1,
        // width: 1
        // }
    },
})

class HeaderBackTitle extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        const { titleText, BackButton, onPress } = this.props
        return (
            <View style={styles.container} elevation={1}>
                <View style={{
                    // flexDirection: 'row',
                    paddingHorizontal: wp(5),
                    width: wp(100),
                    alignItems: 'center',
                    justifyContent: 'space-between'
                }}>
                    {BackButton &&
                        <TouchableOpacity
                            style={{ position: 'absolute', left: wp(5) }}
                            onPress={onPress}
                        >
                            <Image
                                source={assets.icon_back}
                                style={{
                                    resizeMode: 'contain',
                                    justifyContent: 'center',
                                    alignSelf: 'center',
                                    width: wp(3.63),
                                    height: hp(2.1825)
                                }}
                            />
                        </TouchableOpacity>}
                    <Text style={[Font.text12Bold, { color: 'white', textAlign: 'center' }]}>{titleText.toUpperCase()}</Text>
                </View>
            </View>
        )
    }
}

export default HeaderBackTitle
