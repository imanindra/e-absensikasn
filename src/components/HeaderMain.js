import React from 'react'
import { View, Text, StyleSheet, Dimensions, Image, TouchableOpacity } from 'react-native'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import assets from '../assets'
import color from '../constant/Color'
const { width, height } = Dimensions.get('window');
import Font from '../constant/Font'

const styles = StyleSheet.create({
    container: {
        height: height * 0.065,
        // backgroundColor: color.COLOR.YELLOW,
        alignItems: 'center',
        justifyContent: 'center',
        // shadowColor: "#000000",
        // shadowOpacity: 0.8,
        // shadowRadius: 2,
        // shadowOffset: {
        // height: 1,
        // width: 1
        // }
    },
})

class HeaderMain extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        const { titleText, Logout, onPress } = this.props
        return (
            <View style={styles.container} elevation={1}>
                <View style={{
                    // flexDirection: 'row',
                    paddingHorizontal: wp(5),
                    width: wp(100),
                    alignItems: 'center',
                    justifyContent: 'space-between'
                }}>
                    <Text
                        style={[Font.text12Bold, {
                            color: color.COLOR.BLUE,
                            textAlign: 'center'
                        }]}>{titleText}</Text>
                    {Logout &&
                        <TouchableOpacity
                            style={{ position: 'absolute', marginTop: hp(-1), right: wp(5) }}
                            onPress={onPress}
                        >
                            <Image
                                source={assets.icon_logout}
                                style={{
                                    justifyContent: 'center',
                                    alignSelf: 'center',
                                    width: wp(8),
                                    height: wp(8)
                                }}
                            />
                        </TouchableOpacity>}
                </View>
            </View>
        )
    }
}

export default HeaderMain
