import React from 'react'
import { View, Text, TextInput, Image, StyleSheet, TouchableOpacity } from 'react-native'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import CardView from 'react-native-cardview';
import Color from '../constant/Color';
import Font from '../constant/Font';

class MenuHome extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        const { menuText, imageMenu, marginTop, onPress } = this.props
        return (
            <TouchableOpacity
                onPress={onPress}
                style={{
                    alignSelf: 'center',
                    alignContent: 'center',
                    marginTop: marginTop
                }}>
                <CardView
                    style={{
                        height: hp(21.875),
                        width: wp(36.875),
                        paddingVertical: hp(2),
                        zIndex: 999,
                        backgroundColor: Color.COLOR.BLUE,
                        alignSelf: 'center'
                    }}
                    cardElevation={2}
                    cardMaxElevation={2}
                    cornerRadius={10}>
                    <View style={{ alignSelf: 'center' }}>
                        <Text style={[Font.text15BoldCentury, {
                            color: Color.COLOR.WHITE,
                            // marginTop: hp(1),
                            textAlign: 'center'
                        }]}>{menuText}</Text>

                        <Image
                            source={imageMenu}
                            style={{
                                marginTop: hp(2),
                                alignSelf: 'center',
                                resizeMode: 'contain',
                                width: wp(23.125),
                                height: wp(23.125),
                            }}
                        />
                    </View>

                </CardView>
            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({

});


export default MenuHome
