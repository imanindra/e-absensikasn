import React from 'react'
import { View, Text, TextInput, Image, StyleSheet, TouchableOpacity } from 'react-native'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import CardView from 'react-native-cardview';
import Font from '../constant/Font';
import color from '../constant/Color';

class MenuTM extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        const { menuText, imageMenu, marginTop, onPress } = this.props
        return (
            <TouchableOpacity
                onPress={onPress}
                style={{ alignSelf: 'center', marginTop: marginTop }}>
                <CardView
                    style={{
                        width: wp(30.625),
                        height: hp(13.59),
                        zIndex: 999,
                        justifyContent: 'space-around',
                        backgroundColor: color.COLOR.YELLOW,
                    }}
                    cardElevation={2}
                    cardMaxElevation={2}
                    cornerRadius={5}>
                    <Text style={[Font.text10CenturyBold, {
                        textAlign: 'center',
                        color: color.COLOR.WHITE
                    }]}>{menuText}</Text>

                    <Image
                        source={imageMenu}
                        style={{
                            marginTop: hp(-2),
                            alignSelf: 'center',
                            resizeMode: 'contain',
                            width: wp(14.84),
                            height: hp(8.3625),
                        }}
                    />

                </CardView>
            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({

});


export default MenuTM
