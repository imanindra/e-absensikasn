import React from 'react'
import { View, StyleSheet, TouchableOpacity, Text } from 'react-native'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Actions } from 'react-native-router-flux'
import color from '../../constant/Color';



class ButtonText extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        const { status, textButton, pressButton, stylesnya } = this.props
        return (
            <TouchableOpacity
                disabled={status}
                onPress={pressButton}
                style={status == true ? styles.btnPressDisable : styles.btnPress}>
                <Text style={stylesnya}>{textButton}</Text>
            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({
    btnPress: {
        marginTop: hp(5),
        backgroundColor: color.COLOR.BLUE,
        alignItems: 'center',
        paddingVertical: hp(2),
        borderRadius: wp(100) / 2,
        elevation: 5
    },
    btnPressDisable: {
        marginTop: hp(5),
        backgroundColor: '#B8B4B2',
        alignItems: 'center',
        paddingVertical: hp(2),
        borderRadius: wp(2),
        elevation: 5
    }
});


export default ButtonText
