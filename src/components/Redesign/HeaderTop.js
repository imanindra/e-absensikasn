import React from 'react'
import { View, Text, StyleSheet, Dimensions, Image, TouchableOpacity } from 'react-native'
import { widthPercentageToDP as wp, heightPercentageToDP as hp, widthPercentageToDP, heightPercentageToDP } from 'react-native-responsive-screen';
import Constant from '../../constant/Color'
import { Actions } from 'react-native-router-flux';
import assets from '../../assets';
const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
    container: {
        height: height * 0.1,
        backgroundColor: Constant.COLOR.SOFTYELLOW,
        justifyContent: 'center',
        shadowColor: "#000000",
        shadowOpacity: 0.8,
        shadowRadius: 2,
        shadowOffset: {
            height: 1,
            width: 1
        }
    },
})

class Header extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        const { status } = this.props
        return (
            <View style={styles.container} elevation={1}>
                <TouchableOpacity
                    onPress={() => Actions.pop()}
                    style={[styles.titleContainer]}>
                    <Image
                        resizeMode={'contain'}
                        source={assets.icon_verify}
                        style={{
                            marginTop: hp(-2.5),
                            marginHorizontal: wp(5),
                            width: wp(7),
                            height: wp(7),
                            // backgroundColor: 'white'
                        }}
                    />
                </TouchableOpacity>
            </View>
        )
    }
}

export default Header
