export default {
    COLOR: {
        BACKGROUND: '#DFDCFF',
        BLUE: '#001E59',
        SOFTYELLOW: '#EEEED4',
        YELLOW: '#FFBB38',
        ORANGE: '#FF6600',
        GREEN: '#007D04',
        RED: '#FF0000',
        GREYICON: '#CECECE',
        GREYBUTTON: '#707070',
        GREYBT : '#A1A1A1',
        WHITE: '#FFFFFF'
    },
}
