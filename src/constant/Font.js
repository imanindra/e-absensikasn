import { StyleSheet, Dimensions } from 'react-native'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import constant from './Color';
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
const { width, height } = Dimensions.get('window');

export default StyleSheet.create({
    // FontCentury
    text10Century: {
        fontSize: wp(2.4),
        fontFamily: 'CenturyGothic'
    },
    // Bold
    text10CenturyBold: {
        // fontSize: wp(3.5),
        fontSize: RFValue(10, 640),
        fontFamily: 'CenturyGothic',
        // fontWeight: 'bold'
    },
    text15BoldCentury: {
        fontSize: RFValue(15, 640),
        fontFamily: 'CenturyGothic',
        // fontWeight: 'bold'
    },
    text9: {
        fontSize: RFValue(9, 640),
        fontFamily: 'SegoeUI'
    },
    text10: {
        fontSize: RFValue(10, 640),
        fontFamily: 'SegoeUI'
    },
    text10pr: {
        fontSize: RFValue(10, 640),
        fontFamily: 'SegoeUI'
    },
    text10tittle: {
        fontSize: RFValue(10, 640),
        fontFamily: 'SegoeUI'
    },
    text12: {
        fontSize: RFValue(12, 640),
        fontFamily: 'SegoeUI'
    },
    text14: {
        fontSize: RFValue(14, 640),
        fontFamily: 'SegoeUI'
    },
    text16: {
        fontSize: RFValue(16, 640),
        fontFamily: 'SegoeUI'
    },

    // bold
    text8BoldTittle: {
        fontSize: RFValue(8, 640),
        fontFamily: 'Segoe UI Bold'
    },
    text8BoldTittles: {
        fontSize: RFValue(7, 640),
        fontFamily: 'Segoe UI Bold'
    },
    text8Bold: {
        fontSize: RFValue(8, 640),
        fontFamily: 'Segoe UI Bold'
    },
    text8Bolds: {
        fontSize: RFValue(7, 640),
        fontFamily: 'Segoe UI Bold'
    },
    text9Bold: {
        fontSize: RFValue(9, 640),
        fontFamily: 'Segoe UI Bold'
    },
    text10Bold: {
        fontSize: RFValue(10, 640),
        fontFamily: 'Segoe UI Bold'
    },
    text12Bold: {
        // fontSize: wp(3.8),
        // fontSize: wp(4.5),
        fontSize: RFValue(12, 640),
        fontFamily: 'Segoe UI Bold',
        // fontWeight: '900'
    },
    text14Bold: {
        fontSize: RFValue(14, 640),
        fontFamily: 'Segoe UI Bold',
        // fontWeight: '900',
    },
    text16Bold: {
        fontSize: RFValue(16, 640),
        color: constant.COLOR.BLACK,
        fontFamily: 'Segoe UI Bold'
    },
    text20Bold: {
        fontSize: RFValue(20, 640),
        color: constant.COLOR.BLACK,
        fontFamily: 'Segoe UI Bold'
    },
    tex30Bold: {
        fontSize: RFValue(30, 640),
        fontFamily: 'Segoe UI Bold'
    },
    tex50Bold: {
        fontSize: RFValue(50, 640),
        fontFamily: 'Segoe UI Bold'
    },
    text80Bold: {
        fontSize: RFValue(74, 640),
        fontFamily: 'Segoe UI Bold'
    },

    // PROFILE
    text10B: {
        // fontSize: wp(3.8),
        // fontSize: wp(4.5),
        fontSize: RFValue(10, 640),
        fontFamily: 'Segoe UI Bold',
        // fontWeight: '900'
    },
    text10L: {
        // fontSize: wp(3.8),
        // fontSize: wp(4.5),
        fontSize: RFValue(10, 640),
        fontFamily: 'SegoeUI',
    },
})
