import React from 'react';
import { LogBox, SafeAreaView, Alert, BackHandler, View, Text, StyleSheet, TouchableOpacity, Image, AppRegistry, Linking } from 'react-native';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/lib/integration/react';
import AppRouter from './router';
import { store, persistor } from './configureStore';
import codePush from "react-native-code-push";

// console.disableYellowBox = true;
LogBox.ignoreAllLogs(true)

class root extends React.Component {
    state = {
        isModalVisible: false,
        active: true
    };

    toggleModal = () => {
        this.setState({ isModalVisible: !this.state.isModalVisible });
    };

    async UNSAFE_componentDidMount() {

    }

    async UNSAFE_componentWillMount() {
        this.setState({
            isModalVisible: false
        })
    }

    toggleModal = () => {
        this.setState({ isModalVisible: !this.state.isModalVisible, isLoading: false });
    };

    render() {
        const { isModalVisible } = this.state
        return (
            <Provider store={store}>
                <PersistGate persistor={persistor}>
                    <SafeAreaView style={{ flex: 1, backgroundColor: '#FFF' }}>
                        <AppRouter />
                    </SafeAreaView>
                </PersistGate>
            </Provider>
        );
    }
}

// let codePushOptions = { checkFrequency: codePush.CheckFrequency.ON_APP_START, installMode: codePush.InstallMode.IMMEDIATE };
// root = codePush(codePushOptions)(root);
export default root;
// AppRegistry.registerComponent('root', () => root);


// export default root;
// AppRegistry.registerComponent('root', () => root);
