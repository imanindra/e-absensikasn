import React, { Component } from 'react';
import { View, BackHandler, Image, AppRegistry } from 'react-native';
import { Scene, Stack, Router, Tabs, Actions } from 'react-native-router-flux';
import Storage from './data/Storage'
import Login from './screens/login/Login';
import Home from './screens/home/Home';
import Absen from './screens/absen/Absen';
import Profile from './screens/profil/Profile';
import ClockIn from './screens/absen/clockin/ClockIn';
import ClockOut from './screens/absen/clockout/ClockOut';
import LogToday from './screens/absen/LogToday';
import TimeManagement from './screens/timemanagement/TimeManagement';
import DailyReport from './screens/timemanagement/DailyReport';
import MonthReport from './screens/timemanagement/MonthReport';
import SplashScreen from './screens/splashscreen/SplashScreen';

class router extends React.Component {
    state = {
        isInitiated: false,
        token: null,
    }

    async UNSAFE_componentWillMount() {
        await Storage.getUser().then(token => {
            if (token) {
                this.setState({
                    token: token.client_id,
                    isInitiated: true,
                });
                console.log('NULL', this.state.token)
            } else {
                this.setState({ isInitiated: true });
            }
        });
    }


    onBackHandler() {
        const scene = Actions.currentScene
        console.log('Scene', scene)
        if (scene === 'Home') {
            BackHandler.exitApp()
            return true
        }
        Actions.pop()
        return true
    }


    render() {
        const { token, isInitiated } = this.state;
        if (!isInitiated) {
            return null;
        }
        return (
            <View style={{ flex: 1 }}>
                <Router
                    backAndroidHandler={() => this.onBackHandler()}
                >
                    <Scene key="root">
                        <Scene key="SplashScreen" component={SplashScreen} hideNavBar />
                        <Scene key="Login" type="reset" component={Login} hideNavBar />
                        <Scene key="Home" component={Home} hideNavBar />
                        <Scene key="Absen" component={Absen} hideNavBar />
                        <Scene key="ClockIn" component={ClockIn} hideNavBar />
                        <Scene key="ClcokOut" component={ClockOut} hideNavBar />
                        <Scene key="LogToday" component={LogToday} hideNavBar />
                        <Scene key="TimeManagement" component={TimeManagement} hideNavBar />
                        <Scene key="DailyReport" component={DailyReport} hideNavBar />
                        <Scene key="MonthReport" component={MonthReport} hideNavBar />
                        <Scene key="Profile" component={Profile} hideNavBar />
                    </Scene>
                </Router>
            </View>

        )
    }
}
export default router;
