import { call, put, fork, take } from "redux-saga/effects";
import { LOGIN, loginFinish, loginStart, loginSuccess } from "../redux/auth";
// import {fetchProfileSuccess} from '../redux/profile'
import { login } from "../services/api";
import { setErrorMessage } from "../redux/error";
import Storage from "../data/Storage";
import { Actions } from "react-native-router-flux";

function* loginSaga() {
  while (true) {
    const action = yield take(LOGIN);
    try {
      const { user, password } = action.payload;
      console.log("LOGINDATAAAA", user);
      console.log("LOGINDATAAAA", password);
      // console.log('phone_number',phone_number)
      // console.log('phone_number',password)
      yield put(loginStart());
      const loginData = yield call(login, user, password);
      console.log("LOGINDATAAAA", loginData);
      if (__DEV__) {
        console.log("Login data is ", loginData);
      }

      if (loginData.message != "ok") {
        yield put(loginFinish(loginData.message));
        yield put(setErrorMessage(loginData.message));
        alert(loginData.message);
      } else {
        Storage.setUser(loginData);
        // Actions.Home()
        // yield put(fetchProfileSuccess(loginData.data))
        yield put(loginSuccess(loginData));
      }
    } catch (err) {
      console.log("ERROR LOGIN ", err);
    }
  }
}

export default function* wacher() {
  yield fork(loginSaga);
}
