import React, { Component } from "react";
import {
  View,
  Text,
  Image,
  StyleSheet,
  TextInput,
  Platform,
  TouchableOpacity,
} from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import constant from "../../constant/Color";
import HeaderBackTitle from "../../components/HeaderBackTitle";
import moment from "moment";
import CardView from "react-native-cardview";
import Font from "../../constant/Font";
import Color from "../../constant/Color";
import Assets from "../../assets";
import Storage from "../../data/Storage";
import {
  getJamKerja,
  getNameLokasi,
  getClockIn,
  getClockOut,
  getLogToday,
} from "../../services/api";
import { Actions } from "react-native-router-flux";
import Geolocation from "@react-native-community/geolocation";
import assets from "../../assets";

class Absen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      scan_masuk: null,
    };
  }

  async componentDidMount() {
    const Data = await Storage.getUser();
    this.findCoordinates();
    setInterval(() => {
      this.setState({
        curTime: moment().format("HH:mm"),
        curTime2: moment().format("mm"),
      });
    }, 1000);

    this.setState({
      employee_id: Data.employee_id,
      absensi_id: Data.absensi_id,
      ClientID: Data.client_id,
    });
    const { employee_id, absensi_id, ClientID } = this.state;
    await getJamKerja(ClientID, employee_id, absensi_id).then((ok) => {
      this.setState({
        jam_masuk: ok.jam_masuk,
        jam_pulang: ok.jam_pulang,
        date_now: ok.date_now,
        scan_masuk_button: ok.scan_masuk_button,
        scan_masuk_text: ok.scan_masuk_text,
        scan_pulang_button: ok.scan_pulang_button,
        scan_pulang_text: ok.scan_pulang_text,
      });
    });

    await getLogToday(ClientID, employee_id, absensi_id).then((logtoday) => {
      this.setState({
        scan_masuk1: logtoday.scan_masuk,
        scan_pulang: logtoday.scan_pulang,
      });
    });
  }

  // CurrentPosition
  async findCoordinates() {
    Geolocation.getCurrentPosition(
      (success) => {
        this.setState({
          latitude: success.coords.latitude,
          longitude: success.coords.longitude,
        });
        setTimeout(() => {
          this.getLokasi();
        }, 3000);
      },
      (e) => {
        console.log(e);
      },
      { timeout: 20000 }
    );
  }

  async getLokasi() {
    const { latitude, longitude } = this.state;
    // console.log(latitude);
    // console.log(longitude);
    await getNameLokasi(latitude, longitude).then((data) => {
      //   console.log(data);
      console.log("==== ALAMAT =====", JSON.stringify(data));
      this.setState({
        namelokasi: data.results[0].locations[0].street,
        area: data.results[0].locations[0].adminArea3,
      });
    });
  }

  // ----- SCAN MASUK ----- //
  async onScanMasuk() {
    const {
      employee_id,
      absensi_id,
      ClientID,
      latitude,
      longitude,
      namelokasi,
      area,
    } = this.state;
    await getClockIn(ClientID, employee_id, absensi_id).then((data) => {
      console.log("DATA", data);
      this.setState({
        scan_masuk: data.scan_masuk,
        alert_desc: data.alert_desc,
        show_note: data.show_note,
        data: data,
      });
      // console.log('DATA', data.scan_masuk)
      setTimeout(() => {
        Actions.ClockIn({
          data: this.state.data,
          latitude,
          longitude,
          namelokasi,
          area,
        });
      }, 1000);
    });
  }

  // ----- SCAN PULANG ----- //
  async onScanPulang() {
    const {
      employee_id,
      absensi_id,
      ClientID,
      latitude,
      longitude,
      namelokasi,
      area,
    } = this.state;
    await getClockOut(ClientID, employee_id, absensi_id).then((data) => {
      console.log("DATA PULANG", data);
      this.setState({
        scan_masuk: data.scan_masuk,
        alert_desc: data.alert_desc,
        show_note: data.show_note,
        data: data,
      });
      // console.log('DATA', data.scan_masuk)
      setTimeout(() => {
        Actions.ClcokOut({
          data: this.state.data,
          latitude,
          longitude,
          namelokasi,
          area,
        });
      }, 1000);
    });
  }

  render() {
    const {
      scan_masuk_button,
      scan_masuk_text,
      scan_pulang_button,
      scan_pulang_text,
      jam_masuk,
      jam_pulang,
      date_now,
      namelokasi,
      area,
      scan_masuk1,
      scan_pulang,
    } = this.state;
    return (
      <View
        style={{
          // flex: 1,
          backgroundColor: constant.COLOR.YELLOW,
        }}
      >
        <HeaderBackTitle
          BackButton
          onPress={() => Actions.pop()}
          titleText={"Absent"}
        />

        <View
          style={[
            styles.shadowView,
            {
              paddingHorizontal: wp(5),
              // position: 'absolute',
              // height: hp(100),
              marginTop: hp(8),
              backgroundColor: constant.COLOR.SOFTYELLOW,
              borderTopRightRadius: wp(7),
              borderTopLeftRadius: wp(7),
              zIndex: -99,
            },
          ]}
        >
          <View
            style={{
              alignSelf: "center",
              marginTop: Platform.OS == "ios" ? hp(2) : hp(0),
              marginVertical: Platform.OS == "ios" ? hp(1) : hp(3),
              marginBottom: Platform.OS == "ios" ? hp(0) : hp(5),
            }}
          >
            <View
              style={[
                styles.shadowView,
                {
                  flexDirection: "row",
                  backgroundColor: constant.COLOR.YELLOW,
                  borderRadius: wp(5),
                  // paddingHorizontal: wp(5)
                  width: wp(68.61),
                  height: hp(16.25),
                  justifyContent: "center",
                  alignItems: "center",
                },
              ]}
            >
              <Text
                style={[
                  Font.text80Bold,
                  { textAlign: "center", color: constant.COLOR.WHITE },
                ]}
              >
                {this.state.curTime}
              </Text>
              {/* <View style={{
                                borderColor: constant.COLOR.WHITE,
                                borderWidth: 2,
                                marginHorizontal: wp(2),
                                height: hp(10),
                                alignSelf: 'center'
                            }} />
                            <Text style={[Font.text80Bold, { textAlign: 'center', color: constant.COLOR.WHITE }]}>
                                {this.state.curTime2}
                            </Text> */}
            </View>
            <Text
              style={[
                Font.text16,
                styles.shadowView,
                { textAlign: "center", marginTop: hp(1.5) },
              ]}
            >
              {date_now}
            </Text>
          </View>

          <View
            style={[
              styles.shadowView,
              {
                marginTop: Platform.OS == "ios" ? hp(2) : hp(-1),
                height: hp(12.968),
                marginBottom: Platform.OS == "ios" ? hp(9) : hp(10),
                borderWidth: 2,
                borderRadius: wp(3),
                borderColor: constant.COLOR.YELLOW,
                backgroundColor: constant.COLOR.SOFTYELLOW,
              },
            ]}
          >
            <View
              style={{
                position: "absolute",
                alignSelf: "center",
                top: -9,
                paddingHorizontal: wp(1),
                backgroundColor: constant.COLOR.SOFTYELLOW,
              }}
            >
              <Text
                style={[
                  Font.text12Bold,
                  {
                    color: constant.COLOR.YELLOW,
                    textAlign: "center",
                  },
                ]}
              >
                LOKASI
              </Text>
            </View>
            <Text style={{ padding: wp(4) }}>{`${namelokasi},${area}`}</Text>
          </View>
        </View>

        <View
          style={[
            styles.shadowVieww,
            {
              backgroundColor: constant.COLOR.YELLOW,
              marginTop: Platform.OS == "ios" ? hp(-5) : hp(-7),
              borderRadius: wp(7),
              height: hp(100),
              paddingHorizontal: wp(5),
            },
          ]}
        >
          <View
            style={[
              styles.shadowView,
              {
                marginTop: hp(3),
                borderWidth: 2,
                borderRadius: wp(3),
                width: wp(88.89),
                borderColor: constant.COLOR.WHITE,
                backgroundColor: constant.COLOR.YELLOW,
                justifyContent: "center",
                alignItems: "center",
              },
            ]}
          >
            <View
              style={{
                position: "absolute",
                alignSelf: "center",
                top: -9,
                paddingHorizontal: wp(1),
                backgroundColor: constant.COLOR.YELLOW,
              }}
            >
              <Text
                style={[
                  Font.text12Bold,
                  {
                    color: constant.COLOR.WHITE,
                    textAlign: "center",
                  },
                ]}
              >
                JAM KERJA
              </Text>
            </View>

            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-evenly",
                width: wp(88.89),
              }}
            >
              <View
                style={{
                  alignContent: "center",
                  marginVertical: hp(1.5),
                }}
              >
                <Text
                  style={[
                    Font.text12Bold,
                    {
                      textAlign: "center",
                      color: constant.COLOR.WHITE,
                      marginBottom: 3,
                    },
                  ]}
                >
                  Jam Masuk
                </Text>
                {/* <Text style={[Font.tex50Bold]}>{jam_masuk}</Text> */}
                <View
                  style={{
                    borderRadius: wp(3),
                    // flexDirection: 'row',
                    width: wp(28.61),
                    height: hp(6.875),
                    justifyContent: "center",
                    alignItems: "center",
                    backgroundColor: constant.COLOR.WHITE,
                  }}
                >
                  {/* <Text style={[Font.tex50Bold, { color: scan_masuk1 != null ? constant.COLOR.YELLOW : constant.COLOR.WHITE }]}>{scan_masuk1 != null ? scan_masuk1.substring(0, 2) : '--'}</Text> */}
                  <Text
                    style={[Font.tex30Bold, { color: constant.COLOR.YELLOW }]}
                  >
                    {jam_masuk ? jam_masuk : "--"}
                  </Text>
                </View>
              </View>

              {/* Border Middle */}
              <View
                style={{
                  width: wp(1),
                  height: hp(9),
                  alignSelf: "center",
                  // marginVertical: hp(1),
                  borderColor: Color.COLOR.WHITE,
                  borderRightWidth: 2,
                }}
              />
              {/* Border Middle */}

              <View
                style={{
                  alignContent: "center",
                  marginVertical: hp(1),
                }}
              >
                <Text
                  style={[
                    Font.text12Bold,
                    {
                      textAlign: "center",
                      color: constant.COLOR.WHITE,
                      marginBottom: 3,
                    },
                  ]}
                >
                  Jam Pulang
                </Text>
                <View
                  style={{
                    borderRadius: wp(3),
                    // flexDirection: 'row',
                    width: wp(28.61),
                    height: hp(6.875),
                    justifyContent: "center",
                    alignItems: "center",
                    backgroundColor: constant.COLOR.WHITE,
                  }}
                >
                  <Text
                    style={[Font.tex30Bold, { color: constant.COLOR.YELLOW }]}
                  >
                    {jam_pulang ? jam_pulang : "--"}
                  </Text>
                </View>
              </View>
            </View>
          </View>

          <View
            style={{
              borderTopColor: Color.COLOR.YELLOW,
              borderTopWidth: 2,
            }}
          />

          <View
            style={{
              marginTop: Platform.OS == "ios" ? hp(3) : hp(3),
              justifyContent: "space-between",
              flexDirection: "row",
            }}
          >
            <TouchableOpacity
              disabled={
                scan_masuk_button == false && scan_masuk1 == null ? false : true
              }
              style={[
                styles.shadowView,
                {
                  backgroundColor:
                    scan_masuk_button == false && scan_masuk1 == null
                      ? Color.COLOR.BLUE
                      : Color.COLOR.GREYBT,
                  borderRadius: wp(5),
                  width: wp(41.66),
                  height: hp(12.343),
                  justifyContent: "space-evenly",
                  alignItems: "center",
                },
              ]}
              onPress={() => this.onScanMasuk()}
            >
              <Image
                source={
                  scan_masuk_button == false
                    ? Assets.icon_pulang
                    : Assets.icon_masuk_disable
                }
                style={{
                  resizeMode: "contain",
                  // backgroundColor: 'yellow',
                  width: wp(12.5),
                  height: wp(12.5),
                }}
              />
              <Text
                style={[
                  Font.text16Bold,
                  {
                    color:
                      scan_masuk_button == false
                        ? constant.COLOR.WHITE
                        : constant.COLOR.GREYBUTTON,
                  },
                ]}
              >
                {scan_masuk_text}
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              // disabled={scan_pulang_button}
              onPress={() => this.onScanPulang()}
              style={[
                styles.shadowView,
                {
                  backgroundColor:
                    scan_pulang_button == false
                      ? Color.COLOR.BLUE
                      : Color.COLOR.GREYBT,
                  borderRadius: wp(5),
                  width: wp(41.66),
                  height: hp(12.343),
                  justifyContent: "space-evenly",
                  // justifyContent: 'center',
                  alignItems: "center",
                },
              ]}
            >
              <Image
                source={
                  scan_pulang_button == false
                    ? Assets.icon_masuk
                    : Assets.icon_plg_disable
                }
                style={{
                  resizeMode: "contain",
                  width: wp(12.5),
                  height: wp(12.5),
                }}
              />
              <Text
                style={[
                  Font.text16Bold,
                  {
                    color:
                      scan_pulang_button == false
                        ? constant.COLOR.WHITE
                        : constant.COLOR.GREYBUTTON,
                  },
                ]}
              >
                {scan_pulang_text}
              </Text>
            </TouchableOpacity>
          </View>

          <TouchableOpacity
            onPress={() => Actions.LogToday()}
            style={[
              styles.shadowView,
              {
                height: hp(8),
                borderRadius: wp(6),
                backgroundColor: Color.COLOR.BLUE,
                flexDirection: "row",
                justifyContent: "center",
                alignItems: "center",
                marginVertical: Platform.OS == "ios" ? hp(3) : hp(3),
              },
            ]}
          >
            <Image
              source={Assets.icon_logabsen}
              style={{
                resizeMode: "contain",
                width: wp(9.84),
                height: wp(9.84),
              }}
            />
            <Text
              style={[
                Font.text16Bold,
                { color: constant.COLOR.WHITE, marginLeft: wp(3) },
              ]}
            >
              ABSEN LOG TODAY
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  shadowView: {
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
  shadowView1: {
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.29,
    shadowRadius: 4.65,
    elevation: 7,
  },
  shadowVieww: {
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 12,
    },
    shadowOpacity: 0.58,
    shadowRadius: 16.0,

    elevation: 24,
  },
});

export default Absen;
