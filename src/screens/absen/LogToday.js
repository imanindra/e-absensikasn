import React, { Component } from "react";
import { View, Text, Image, StyleSheet, TextInput } from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import Color from "../../constant/Color";
import Font from "../../constant/Font";
import Storage from "../../data/Storage";
import { getLogToday } from "../../services/api";
import HeaderBackTitle from "../../components/HeaderBackTitle";
import { Actions } from "react-native-router-flux";
import ButtonText from "../../components/ButtonText";

class LogToday extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  async componentDidMount() {
    const Data = await Storage.getUser();
    this.setState({
      employee_id: Data.employee_id,
      absensi_id: Data.absensi_id,
      ClientID: Data.client_id,
    });
    const { employee_id, absensi_id, ClientID } = this.state;
    console.log(ClientID);
    console.log("______", employee_id);
    console.log("____2____", absensi_id);
    await getLogToday(ClientID, employee_id, absensi_id).then((ok) => {
      console.log("== GET LOG TODAY1 ==", ok);
      this.setState({
        tanggal: ok.tanggal,
        jam_kerja: ok.jam_kerja,
        scan_masuk: ok.scan_masuk,
        scan_pulang: ok.scan_pulang,
        terlambat: ok.terlambat,
        pulang_awal: ok.pulang_awal,
        jumlah_jam_kerja: ok.jumlah_jam_kerja,
        keterangan: ok.keterangan,
      });
    });
  }

  render() {
    const {
      tanggal,
      jam_kerja,
      scan_masuk,
      scan_pulang,
      terlambat,
      pulang_awal,
      jumlah_jam_kerja,
      keterangan,
    } = this.state;
    return (
      <View
        style={{
          flex: 1,
          backgroundColor: Color.COLOR.YELLOW,
        }}
      >
        <HeaderBackTitle
          BackButton
          onPress={() => Actions.pop()}
          titleText={"ABSEN LOG TODAY"}
        />
        <View
          style={{
            paddingHorizontal: wp(5),
            height: hp(100),
            marginTop: hp(8),
            backgroundColor: Color.COLOR.SOFTYELLOW,
            borderTopRightRadius: wp(7),
            borderTopLeftRadius: wp(7),
          }}
        >
          <View style={{ flexDirection: "row" }}>
            <Text style={[Font.text10tittle, styles.tittleLeft]}>Tanggal</Text>
            <Text
              style={[Font.text10tittle, styles.tittleRight]}
            >{`: ${tanggal}`}</Text>
          </View>
          <View style={{ flexDirection: "row" }}>
            <Text style={[Font.text10tittle, styles.tittleLeft]}>
              Jam Kerja
            </Text>
            <Text
              style={[Font.text10tittle, styles.tittleRight]}
            >{`: ${jam_kerja}`}</Text>
          </View>
          <View style={{ flexDirection: "row" }}>
            <Text style={[Font.text10tittle, styles.tittleLeft]}>
              Scan Masuk
            </Text>
            <Text
              style={[Font.text10tittle, styles.tittleRight]}
            >{`: ${scan_masuk}`}</Text>
          </View>
          <View style={{ flexDirection: "row" }}>
            <Text style={[Font.text10tittle, styles.tittleLeft]}>
              Scan Pulang
            </Text>
            <Text
              style={[Font.text10tittle, styles.tittleRight]}
            >{`: ${scan_pulang} `}</Text>
          </View>
          <View style={{ flexDirection: "row" }}>
            <Text style={[Font.text10tittle, styles.tittleLeft]}>
              Terlambat
            </Text>
            <Text
              style={[Font.text10tittle, styles.tittleRight]}
            >{`: ${terlambat}`}</Text>
          </View>
          <View style={{ flexDirection: "row" }}>
            <Text style={[Font.text10tittle, styles.tittleLeft]}>
              Pulang Awal
            </Text>
            <Text
              style={[Font.text10tittle, styles.tittleRight]}
            >{`: ${pulang_awal}`}</Text>
          </View>
          <View style={{ flexDirection: "row" }}>
            <Text style={[Font.text10tittle, styles.tittleLeft]}>
              Jumlah Jam Kerja
            </Text>
            <Text
              style={[Font.text10tittle, styles.tittleRight]}
            >{`: ${jumlah_jam_kerja}`}</Text>
          </View>
          <View style={{ flexDirection: "row" }}>
            <Text style={[Font.text10tittle, styles.tittleLeft]}>
              Keterangan
            </Text>
            <Text
              style={[Font.text10tittle, styles.tittleRight]}
            >{`: ${keterangan}`}</Text>
          </View>

          <View style={{ width: wp(51.94), alignSelf: "center" }}>
            <ButtonText
              pressButton={() => Actions.pop()}
              textButton="OKE"
              stylesnya={[Font.text16Bold, { color: Color.COLOR.WHITE }]}
            />
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  tittleLeft: {
    width: wp(35),
    marginTop: hp(1.5),
    color: Color.COLOR.GREYBUTTON,
  },
  tittleRight: {
    width: wp(54),
    borderBottomWidth: 1,
    color: Color.COLOR.BLUE,
    marginTop: hp(1.5),
  },
});

export default LogToday;
