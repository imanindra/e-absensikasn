import React, { Component } from "react";
import {
  View,
  Text,
  Image,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  PermissionsAndroid,
  Dimensions,
} from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import constant from "../../../constant/Color";
import HeaderBackTitle from "../../../components/HeaderBackTitle";
import moment from "moment";
import CardView from "react-native-cardview";
import Font from "../../../constant/Font";
// import Color from '../../../constant/Color';
import Storage from "../../../data/Storage";
import { clockOut } from "../../../services/api";
import assets from "../../../assets";
import ButtonText from "../../../components/ButtonText";
import { Actions } from "react-native-router-flux";
import { launchCamera, launchImageLibrary } from "react-native-image-picker";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";

class ClockOut extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selfie: "",
    };
  }

  async componentDidMount() {
    const { data } = this.props;
    console.log("...", data);
    const Data = await Storage.getUser();
    setInterval(() => {
      this.setState({
        curTime: moment().format("HH"),
        curTime2: moment().format("mm"),
        scan_pulang: moment().format("HH:mm"),
      });
    }, 1000);

    this.setState({
      employee_id: Data.employee_id,
      absensi_id: Data.absensi_id,
      ClientID: Data.client_id,
      tanggal: moment().format("YYYY-MM-DD"),
      // NEW DATA PROPS ABSEN
      // scan_pulang: data.scan_pulang,
      alert_desc: data.alert_desc,
      show_note: data.show_note,
      note: data.note,
      date_now: data.date_now,
    });
  }

  async chooseFile() {
    const grantedcamera = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.CAMERA,
      {
        title: "App Camera Permission",
        message: "App needs access to your camera ",
        buttonNeutral: "Ask Me Later",
        buttonNegative: "Cancel",
        buttonPositive: "OK",
      }
    );
    const grantedstorage = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
      {
        title: "App Camera Permission",
        message: "App needs access to your camera ",
        buttonNeutral: "Ask Me Later",
        buttonNegative: "Cancel",
        buttonPositive: "OK",
      }
    );

    if (
      grantedcamera === PermissionsAndroid.RESULTS.GRANTED &&
      grantedstorage === PermissionsAndroid.RESULTS.GRANTED
    ) {
      console.log("Camera & storage permission given");

      const options = {
        title: "photo selfie",
        cameraType: "front",
        storageOptions: {
          skipBackup: true,
          path: "images",
        },
        quality: 0.15,
        includeBase64: true,
      };

      launchCamera(options, (response) => {
        if (response.didCancel) {
          console.log("User cancelled image picker");
        } else if (response.error) {
          console.log("ImagePicker Error: ", response.error);
        } else if (response.customButton) {
          console.log("User tapped custom button: ", response.customButton);
        } else {
          const source = { uri: response.assets[0].uri };
          // this.onUpdatePhoto(response);
          console.log("response", JSON.stringify(response));
          this.setState({
            selfie: source,
            filePath: source,
            fileName:
              response.fileName == undefined
                ? "Photo"
                : response.assets[0]?.fileName,
            types: response.assets[0]?.type,
            base64: response.assets[0].base64,
          });
        }
      });
    }
  }

  formValidation() {
    const { selfie, note } = this.state;
    if (note === "") {
      alert("Keterangan Tidak Boleh Kosong");
      return false;
    }
    if (selfie === "") {
      alert("Selfie tidak boleh kosong");
      return false;
    }
    return true;
  }

  async onScanPulang() {
    const { latitude, longitude, namelokasi, area } = this.props;
    const {
      ClientID,
      absensi_id,
      employee_id,
      tanggal,
      scan_pulang,
      base64,
      note,
    } = this.state;
    if (this.formValidation()) {
      await clockOut(
        ClientID,
        absensi_id,
        employee_id,
        `${tanggal} ${scan_pulang}`,
        latitude,
        longitude,
        `${namelokasi},${area}`,
        base64,
        note
      ).then((data) => {
        console.log("[data TANGGAL]", data);
        if (data.code == 0) {
          Actions.Home();
        } else {
          alert(data.message);
        }
      });
    }
  }

  render() {
    // const { scan_masuk, alert_desc, show_note } = this.props
    const {
      curTime,
      curTime2,
      jam_masuk,
      jam_pulang,
      date_now,
      scan_pulang,
      alert_desc,
      show_note,
    } = this.state;
    // console.log('SCAN PULANG', scan_pulang)
    return (
      <KeyboardAwareScrollView
        style={{
          flex: 1,
          height: Dimensions.get("window").height,
          width: Dimensions.get("window").width,
        }}
      >
        <View
          style={{
            // flex: 1,
            backgroundColor: constant.COLOR.YELLOW,
          }}
        >
          <HeaderBackTitle
            BackButton
            onPress={() => Actions.pop()}
            titleText={"Scan Pulang"}
          />

          <View
            style={[
              styles.shadowView,
              {
                marginTop: hp(8),
                backgroundColor: constant.COLOR.SOFTYELLOW,
                borderTopRightRadius: wp(7),
                borderTopLeftRadius: wp(7),
              },
            ]}
          >
            {/* <View style={{ height: hp(13), marginVertical: hp(3), marginBottom: hp(5) }}>
                        <Text style={[Font.text80Bold, { textAlign: 'center' }]}>
                            {scan_pulang}
                        </Text>
                        <Text style={[Font.text16, { textAlign: 'center' }]}>
                            {date_now}
                        </Text>
                    </View> */}

            <View
              style={{
                alignSelf: "center",
                // marginTop: hp(2),
                marginVertical: Platform.OS == "ios" ? hp(1) : hp(3),
                marginBottom: Platform.OS == "ios" ? hp(0) : hp(5),
              }}
            >
              <View
                style={[
                  styles.shadowView,
                  {
                    // paddingHorizontal: wp(5),
                    // flexDirection: 'row',
                    // backgroundColor: constant.COLOR.YELLOW,
                    // borderRadius: wp(5),
                    // paddingHorizontal: wp(5)
                    flexDirection: "row",
                    backgroundColor: constant.COLOR.YELLOW,
                    borderRadius: wp(5),
                    width: wp(68.61),
                    height: hp(16.25),
                    justifyContent: "center",
                    alignItems: "center",
                  },
                ]}
              >
                <Text
                  style={[
                    Font.text80Bold,
                    { textAlign: "center", color: constant.COLOR.WHITE },
                  ]}
                >
                  {scan_pulang}
                </Text>
              </View>
              <Text
                style={[
                  Font.text16,
                  { textAlign: "center", marginTop: hp(1.5) },
                ]}
              >
                {date_now}
              </Text>
            </View>

            <View
              style={[
                styles.shadowView,
                {
                  marginTop: hp(-2),
                  backgroundColor: constant.COLOR.YELLOW,
                  width: wp(100),
                  height: hp(100),
                  paddingHorizontal: wp(5),
                  borderTopLeftRadius: wp(7),
                  borderTopRightRadius: wp(7),
                },
              ]}
            >
              <CardView
                style={{
                  alignSelf: "center",
                  marginTop: hp(4),
                  width: wp(52.22),
                  height: hp(10),
                }}
                cardElevation={2}
                cardMaxElevation={2}
                cornerRadius={5}
              >
                <View
                  style={{
                    alignSelf: "center",
                    // justifyContent: 'center',
                    // height: hp(13),
                  }}
                >
                  <Image
                    source={
                      show_note == false
                        ? assets.icon_warning
                        : assets.icon_checklist
                    }
                    style={{
                      width: wp(9.68),
                      height: wp(9.68),
                      alignSelf: "center",
                      marginVertical: hp(1.2),
                    }}
                  />
                  {show_note == false && (
                    <Text
                      style={[Font.text9Bold, { color: constant.COLOR.BLUE }]}
                    >
                      {alert_desc}
                    </Text>
                  )}
                </View>
              </CardView>

              {this.state.selfie == "" ? (
                <TouchableOpacity onPress={this.chooseFile.bind(this)}>
                  <Image
                    source={assets.icon_camera}
                    style={{
                      marginTop: hp(3),
                      resizeMode: "contain",
                      height: wp(30),
                      width: wp(30),
                      alignSelf: "center",
                    }}
                  />
                </TouchableOpacity>
              ) : (
                <TouchableOpacity onPress={this.chooseFile.bind(this)}>
                  <Image
                    source={this.state.selfie}
                    style={{
                      marginTop: hp(3),
                      marginBottom: hp(-1),
                      // resizeMode: 'contain',
                      height: wp(30),
                      width: wp(30),
                      borderRadius: wp(30) / 2,
                      alignSelf: "center",
                    }}
                  />
                </TouchableOpacity>
              )}

              <View
                style={{
                  borderRadius: wp(3),
                  marginTop: hp(5),
                  height: hp(12),
                  borderColor: show_note == true ? "white" : "#707070",
                  borderWidth: 2,
                  marginBottom: hp(-2),
                }}
              >
                <View
                  style={{
                    position: "absolute",
                    left: 20,
                    top: -10,
                    backgroundColor: constant.COLOR.YELLOW,
                    paddingHorizontal: wp(2),
                  }}
                >
                  <Text
                    style={[
                      Font.text12Bold,
                      { color: show_note == true ? "white" : "#707070" },
                    ]}
                  >
                    Keterangan
                  </Text>
                </View>

                <TextInput
                  // editable={show_note}
                  style={[
                    Font.text12,
                    {
                      marginTop: hp(-1),
                      paddingHorizontal: wp(5),
                      color: "white",
                    },
                  ]}
                  onChangeText={(text) => this.setState({ note: text })}
                  multiline={true}
                  numberOfLines={2}
                />
              </View>

              <View style={{ width: wp(51.94), alignSelf: "center" }}>
                <ButtonText
                  pressButton={() => this.onScanPulang()}
                  status={this.state.selfie == "" && true}
                  textButton="SEND"
                  stylesnya={{ color: constant.COLOR.WHITE }}
                />
              </View>
            </View>
          </View>
        </View>
      </KeyboardAwareScrollView>
    );
  }
}

const styles = StyleSheet.create({
  shadowView: {
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
});

export default ClockOut;
