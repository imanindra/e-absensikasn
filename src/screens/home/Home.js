import React, { Component } from 'react';
import {
    View,
    Text,
    Image,
    StyleSheet,
    TextInput,
    Platform,
    ImageBackground
} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import assets from '../../assets';
import constant from '../../constant/Color';
import HeaderBack from '../../components/HeaderBack';
import CardView from 'react-native-cardview';
import ButtonText from '../../components/ButtonText';
import HeaderMain from '../../components/HeaderMain';
import MenuHome from '../../components/MenuHome';
import MenuTM from '../../components/MenuTM';
import Font from '../../constant/Font';
import { getLogout } from '../../services/api';
import { Actions } from 'react-native-router-flux';
import Storage from '../../data/Storage'

class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    async componentDidMount() {
        const Data = await Storage.getUser();
        console.log('==DATA==', Data)
        this.setState({
            nama: Data.employee_profile.nama,
            unit_kerja: Data.employee_profile.unit_kerja,
            employee_id: Data.employee_id,
            absensi_id: Data.absensi_id,
            ClientID: Data.client_id,
        })
    }

    async onLogout() {
        const { ClientID, employee_id, absensi_id } = this.state
        await getLogout(ClientID, employee_id).then(data => {
            console.log('==== LOGOUT =====', data)
            this.setState({

            })
            Storage.resetUser();
            Actions.Login()
        })
    }

    render() {
        const { nama, unit_kerja } = this.state
        return (
            <View style={{
                flex: 1,
                backgroundColor: constant.COLOR.BACKGROUND
            }}>
                <ImageBackground
                    // resizeMode={'stretch'}
                    source={assets.bg_custome}
                    style={{ width: wp(100), height: hp(100) }}
                >
                    <HeaderMain
                        Logout
                        onPress={() => this.onLogout()}
                        titleText={'EMPLOYEE MANAGEMENT'}
                    />

                    <View style={{
                        // backgroundColor: constant.COLOR.YELLOW,
                        height: hp(35),
                        alignItems: 'center'
                    }}>
                        <Image
                            source={assets.icon_logo}
                            style={{
                                resizeMode: 'contain',
                                width: wp(49.375),
                                height: hp(21.093),
                                marginBottom: hp(2),
                            }}
                        />
                        <Text style={[Font.text14Bold, { color: 'white' }]}>{nama}</Text>
                        <Text style={[Font.text12, { color: 'white' }]}>{unit_kerja}</Text>
                    </View>

                    <View style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        marginTop: hp(-2),
                        paddingHorizontal: wp(7),
                    }}>

                        <MenuHome
                            onPress={() => Actions.Absen()}
                            // marginTop={hp(2)}
                            imageMenu={assets.icon_absent}
                            menuText={'ABSENT'}
                        />

                        <MenuHome
                            onPress={() => Actions.Profile()}
                            // marginTop={Platform.OS == 'ios' ? hp(-10) : hp(-15)}
                            imageMenu={assets.icon_profile}
                            menuText={'MY PROFILE'}
                        />
                    </View>


                    <View>
                        <View style={[styles.shadowView,
                        {
                            backgroundColor: constant.COLOR.SOFTYELLOW,
                            flexDirection: 'row',
                            justifyContent: 'center',
                            marginTop: hp(5),
                            borderRadius: wp(5),
                            borderColor: constant.COLOR.BLUE,
                            borderWidth: 2,
                            alignSelf: 'center',
                            padding: wp(5),
                            width: wp(85),
                        }]}>
                            <View style={{
                                position: 'absolute',
                                top: -10,
                                alignItems: 'center',
                                backgroundColor: constant.COLOR.SOFTYELLOW
                            }}>
                                <Text style={[Font.text15BoldCentury, { color: constant.COLOR.BLUE, textAlign: 'center' }]}>TIME MANAGEMENT</Text>
                            </View>

                            <View style={{ flexDirection: 'row' }}>
                                <MenuTM
                                    onPress={() => Actions.DailyReport()}
                                    imageMenu={assets.icon_day}
                                    menuText={'DAILY REPORT'}
                                />
                                <View style={{ marginHorizontal: wp(4) }} />
                                <MenuTM
                                    onPress={() => Actions.MonthReport()}
                                    imageMenu={assets.icon_month}
                                    menuText={'SUMMARY REPORT'}
                                />
                            </View>
                        </View>
                    </View>
                </ImageBackground>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    logo: {
        resizeMode: 'contain',
        width: wp(55),
        height: hp(30)
    },
    shadowView: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
    }
});

export default Home;
