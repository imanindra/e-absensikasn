import React, { Component } from "react";
import {
  View,
  Text,
  Image,
  StyleSheet,
  TextInput,
  Platform,
} from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import assets from "../../assets";
import constant from "../../constant/Color";
import ButtonText from "../../components/Redesign/ButtonText";
import { Actions } from "react-native-router-flux";
import { connect } from "react-redux";
import {
  login,
  loginFinish,
  getIsSuccess,
  getIsFetching,
  getMessage,
} from "../../redux/auth";
import Font from "../../constant/Font";

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      // user: "123456",
      // password: "123456",
      user: "",
      password: "",
      isLoading: false,
      isErrorValidation: false,
      errorMsgField: "",
    };
  }

  // Function Aplikasi
  async UNSAFE_componentWillReceiveProps(nextProps) {
    const { isFetching, isSuccess, dispatch } = nextProps;
    if (isSuccess) {
      // alert('tes')
      dispatch(loginFinish());
      // const data = await Storage.getUser();
      Actions.Home();
    }
  }

  onLogin() {
    const { dispatch } = this.props;
    const { user, password } = this.state;
    console.log(user);
    console.log(password);
    if (user === "") {
      alert("NIP tidak boleh kosong");
    } else if (password === "") {
      alert("Password tidak boleh kosong");
    } else {
      dispatch(login({ user, password }));
    }
  }

  render() {
    return (
      <View
        style={{
          flex: 1,
          backgroundColor: constant.COLOR.SOFTYELLOW,
        }}
      >
        <View style={{ flexDirection: "row" }}>
          <Image
            resizeMode={"contain"}
            source={assets.icon_verify}
            style={{
              marginVertical: wp(2),
              marginLeft: wp(4),
              marginBottom: wp(4),
              width: wp(11.25),
              height: wp(11.25),
            }}
          />
        </View>

        <View
          style={{
            flex: 1,
            borderTopLeftRadius: wp(7),
            borderTopRightRadius: wp(7),
            backgroundColor: constant.COLOR.YELLOW,
          }}
        >
          <View style={{ alignSelf: "center", marginTop: hp(-7) }}>
            <Image source={assets.icon_logo} style={styles.logo} />
          </View>
          <View style={{ alignItems: "center", marginTop: hp(10) }}>
            <View>
              <View
                style={[
                  styles.shadowView,
                  {
                    borderRadius: wp(2),
                    height: hp(5.156),
                    borderWidth: 1,
                    backgroundColor: constant.COLOR.YELLOW,
                    borderColor: constant.COLOR.WHITE,
                    width: wp(90),
                  },
                ]}
              >
                <View
                  style={{
                    backgroundColor: constant.COLOR.YELLOW,
                    position: "absolute",
                    left: wp(3),
                    top: hp(-1),
                    zIndex: 9999,
                    paddingHorizontal: wp(1),
                  }}
                >
                  <Text style={[Font.text12, { color: constant.COLOR.WHITE }]}>
                    Username
                  </Text>
                </View>
                <TextInput
                  // placeholder={'Username'}
                  keyboardType={"number-pad"}
                  style={[
                    Font.text12,
                    {
                      paddingLeft: wp(8),
                      // Ios Build
                      height: hp(5),
                    },
                  ]}
                  // style={{ padding: Platform.OS == 'ios' ? hp(2) : hp(1.5) }}
                  onChangeText={(text) => this.setState({ user: text })}
                  autoCapitalize={"characters"}
                />
              </View>
            </View>

            <View style={{ marginTop: hp(4) }}>
              <View
                style={[
                  styles.shadowView,
                  {
                    borderRadius: wp(2),
                    height: hp(5.156),
                    borderWidth: 1,
                    backgroundColor: constant.COLOR.YELLOW,
                    borderColor: constant.COLOR.WHITE,
                    width: wp(90),
                  },
                ]}
              >
                <View
                  style={{
                    backgroundColor: constant.COLOR.YELLOW,
                    position: "absolute",
                    left: wp(3),
                    top: hp(-1),
                    zIndex: 99,
                    paddingHorizontal: wp(1),
                  }}
                >
                  <Text style={[Font.text12, { color: constant.COLOR.WHITE }]}>
                    Password
                  </Text>
                </View>
                <TextInput
                  // placeholder={'Password'}
                  style={[
                    Font.text12,
                    {
                      paddingLeft: wp(8),
                      // Ios Build
                      height: hp(5),
                    },
                  ]}
                  // style={{ padding: Platform.OS == 'ios' ? hp(2) : hp(1.5) }}
                  onChangeText={(text) => this.setState({ password: text })}
                  autoCapitalize={"none"}
                />
              </View>
            </View>

            <View
              style={{
                alignSelf: "center",
                width: wp(47.5),
                paddingHorizontal: wp(5),
                marginTop: hp(-1),
              }}
            >
              <ButtonText
                pressButton={() => this.onLogin()}
                textButton={"LOGIN"}
                stylesnya={[Font.text12Bold, { color: "white" }]}
              />
            </View>

            <View
              style={{
                alignSelf: "center",
                width: wp(100),
                alignItems: "center",
                padding: wp(5),
              }}
            >
              <Text style={[Font.text12, { color: constant.COLOR.WHITE }]}>
                Supported by
              </Text>
              <Text style={[Font.text12, { color: constant.COLOR.WHITE }]}>
                PT Inspiratif Solusi Teknologi
              </Text>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const mapStateToProps = (state) => ({
  isFetching: getIsFetching(state.auth),
  isSuccess: getIsSuccess(state.auth),
  // hasError: getHasError(state.error),
  // message: getErrorMessage(state.error),
  // defaultMessage: getDefaultMessage(state.error),
});

const styles = StyleSheet.create({
  logo: {
    resizeMode: "contain",
    width: wp(56.54),
    height: hp(23.59),
  },
  shadowView: {
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    zIndex: 0,
    elevation: 5,
  },
});

export default connect(mapStateToProps)(Login);
