import React, { Component } from 'react';
import {
    View,
    Text,
    Image,
    StyleSheet,
    TextInput,
    TouchableOpacity
} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import assets from '../../assets';
import constant from '../../constant/Color';
import HeaderBackTittle from '../../components/HeaderBackTitle';
import Font from '../../constant/Font';
import { TabView, SceneMap } from 'react-native-tab-view';
import ProfilePegawai from './TabProfile/ProfilePegawai';
import AbsenTukin from './TabProfile/AbsenTukin';
import Password from './TabProfile/Password';
import UnitKerja from './TabProfile/UnitKerja';

import { Actions } from 'react-native-router-flux';

class Profile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            active: 1
        };
    }

    render() {
        const { active } = this.state
        return (
            <View style={{
                flex: 1,
                backgroundColor: constant.COLOR.YELLOW
            }}>
                <HeaderBackTittle
                    onPress={() => Actions.pop()}
                    BackButton
                    titleText={'MY PROFILE'}
                />
                <View style={{
                    marginTop: hp(8),
                    // backgroundColor : 'red',
                    paddingLeft: wp(3),
                    flexDirection: 'row',
                    justifyContent: 'space-between'
                }}>

                    <TouchableOpacity
                        style={{ alignItems: 'center', width: wp(20) }}
                        onPress={() => this.setState({ active: 1 })}
                    >
                        <Image
                            style={{
                                resizeMode: 'contain',
                                height: wp(6.67),
                                width: wp(6.67),
                                // paddingHorizontal: wp(2),
                                backgroundColor: constant.COLOR.YELLOW,
                                marginBottom: hp(1)
                            }}
                            source={active == 1 ? assets.icon_pg_active : assets.icon_pg_disable}
                        />
                        {active == 1 ?
                            <Text style={[Font.text10B, { color: constant.COLOR.WHITE }]}>Profil Pegawai</Text>
                            :
                            <Text style={[Font.text10L, { color: constant.COLOR.GREYBUTTON }]}>Profil Pegawai</Text>
                        }
                    </TouchableOpacity>

                    <View
                        style={{
                            position: 'absolute',
                            // left: wp(20),
                            zIndex: -1,
                            marginTop: hp(1.2),
                            justifyContent: 'center',
                            marginHorizontal: wp(18),
                            width: wp(70),
                            height: 1,
                            borderWidth: 1,
                            borderColor: '#707070'
                        }}
                    />

                    <TouchableOpacity
                        style={{ alignItems: 'center', width: wp(20) }}
                        onPress={() => this.setState({ active: 2 })}
                    >
                        <Image
                            style={{
                                resizeMode: 'contain',
                                height: wp(6.67),
                                width: wp(6.67),
                                paddingHorizontal: wp(5),
                                backgroundColor: constant.COLOR.YELLOW,
                                marginBottom: hp(1)
                            }}
                            source={active == 2 ? assets.icon_uk_active : assets.icon_uk_disable}
                        />
                        {active == 2 ?
                            <Text style={[Font.text10B, { color: constant.COLOR.WHITE }]}>Unit Kerja</Text>
                            :
                            <Text style={[Font.text10L, { color: constant.COLOR.GREYBUTTON }]}>Unit Kerja</Text>
                        }
                        {/* <Text>Unit Kerja</Text> */}
                    </TouchableOpacity>

                    {/* <View
                        style={{
                            position: 'absolute',
                            left: wp(44),
                            marginTop: hp(1),
                            justifyContent: 'center',
                            width: wp(15),
                            height: 1,
                            borderWidth: 1
                        }}
                    /> */}
                    <TouchableOpacity
                        style={{ alignItems: 'center', width: wp(20) }}
                        onPress={() => this.setState({ active: 3 })}
                    >
                        <Image
                            style={{
                                resizeMode: 'contain',
                                height: wp(6.67),
                                width: wp(6.67),
                                paddingHorizontal: wp(5),
                                backgroundColor: constant.COLOR.YELLOW,
                                marginBottom: hp(1)
                            }}
                            source={active == 3 ? assets.icon_ak_active : assets.icon_ak_disable}
                        />
                        {active == 3 ?
                            <Text style={[Font.text10B, { color: constant.COLOR.WHITE }]}>Absen & Tukin</Text>
                            :
                            <Text style={[Font.text10L, { color: constant.COLOR.GREYBUTTON }]}>Absen & Tukin</Text>
                        }
                        {/* <Text>Absen & Tukin</Text> */}
                    </TouchableOpacity>

                    {/* <View
                        style={{
                            position: 'absolute',
                            left: wp(68.5),
                            marginTop: hp(1),
                            justifyContent: 'center',
                            width: wp(15),
                            height: 1,
                            borderWidth: 1
                        }}
                    /> */}

                    <TouchableOpacity
                        style={{ alignItems: 'center', width: wp(20) }}
                        onPress={() => this.setState({ active: 4 })}
                    >
                        <Image
                            style={{
                                resizeMode: 'contain',
                                height: wp(6.67),
                                width: wp(6.67),
                                paddingHorizontal: wp(5),
                                backgroundColor: constant.COLOR.YELLOW,
                                marginBottom: hp(1)
                            }}
                            source={active == 4 ? assets.icon_pwd_active : assets.icon_pwd_disable}
                        />
                        {active == 4 ?
                            <Text style={[Font.text10B, { color: constant.COLOR.WHITE }]}>Password</Text>
                            :
                            <Text style={[Font.text10L, { color: constant.COLOR.GREYBUTTON }]}>Password</Text>
                        }
                    </TouchableOpacity>
                </View>
                <View style={[styles.shadowView1,
                {
                    backgroundColor: constant.COLOR.SOFTYELLOW,
                    flex: 1,
                    marginTop: hp(2),
                    borderTopLeftRadius: wp(7),
                    borderTopRightRadius: wp(7)
                }]}>
                    {active == 1 && <ProfilePegawai />}
                    {active == 2 && <UnitKerja />}
                    {active == 3 && <AbsenTukin />}
                    {active == 4 && <Password />}
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    activeText: {
        color: constant.COLOR.WHITE,
    },
    disableText: {
        color: '#FFFFFF'
    },
    shadowView: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
    },
    shadowView1: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.29,
        shadowRadius: 4.65,

        elevation: 7,

    }

});

export default Profile;
