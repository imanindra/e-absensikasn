import * as React from 'react';
import { View, StyleSheet, Dimensions, StatusBar, Text } from 'react-native';
import { TabView, SceneMap, TabBar } from 'react-native-tab-view'
import Font from '../../constant/Font'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Color from '../../constant/Color';
import ProfilePegawai from '../profil/TabProfile/ProfilePegawai';
import UnitKerja from '../profil/TabProfile/UnitKerja';
import AbsenTukin from '../profil/TabProfile/AbsenTukin';
import Password from '../profil/TabProfile/Password';

const FirstRoute = () => (
    <ProfilePegawai />
);

const SecondRoute = () => (
    <UnitKerja />
);

const ThirdRoute = () => (
    <AbsenTukin />
);
const FourRoute = () => (
    <Password />
);



export default class TabProfile extends React.Component {
    state = {
        index: 0,
        routes: [
            { key: 'first', title: 'Profile Pegawai' },
            { key: 'second', title: 'Unit Kerja' },
            { key: 'third', title: 'Absen & Tukin' },
            { key: 'four', title: 'Password' },

        ],
    };

    renderLabel = ({ route, focused, color }) => {
        return (
            <View>
                <Text
                    style={[focused ? Font.text10Bold : Font.text10]}
                >
                    {route.title}
                </Text>
            </View>
        )
    }

    render() {
        // alert(this.state.index)
        return (
            <View style={{ flex: 1 }}>
                <TabView
                    navigationState={this.state}
                    renderScene={SceneMap({
                        first: FirstRoute,
                        second: SecondRoute,
                        third: ThirdRoute,
                        four: FourRoute
                    })}

                    onIndexChange={index => this.setState({ index })}
                    initialLayout={{ width: Dimensions.get('window').width }}
                    renderTabBar={props =>
                        <TabBar
                            {...props}
                            style={[Font.text10pr, {
                                backgroundColor: '#DFDCFF',
                                width: wp(100),
                                alignSelf: 'center',
                                // borderRadius: 10
                            }]}
                            renderLabel={this.renderLabel}
                            // style={[focused ? styles.activeTabTextColor : styles.tabTextColor]}
                            labelStyle={[Font.text10, { color: Color.COLOR.BLUE }]}
                            indicatorStyle={{ backgroundColor: Color.COLOR.YELLOW }}
                        />

                    }
                    style={styles.container}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        // marginTop: StatusBar.currentHeight,
    },
    scene: {
        flex: 1,
    },
    activeTabTextColor: {

    }
});
