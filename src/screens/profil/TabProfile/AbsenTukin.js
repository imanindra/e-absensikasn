import React, { Component } from 'react';
import {
    View,
    Text,
    Image,
    StyleSheet,
    TextInput
} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Color from '../../../constant/Color';
import Font from '../../../constant/Font';
import Storage from '../../../data/Storage';

class AbsenTukin extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    async componentDidMount() {
        const Data = await Storage.getUser()
        this.setState({
            approval_pertama: Data.employee_profile.approval_pertama,
            approval_kedua: Data.employee_profile.approval_kedua,
            potongan_terlambat: Data.employee_profile.potongan_terlambat,
            potongan_pulang_awal: Data.employee_profile.potongan_pulang_awal,
            potongan_lupa_absen_pagi: Data.employee_profile.potongan_lupa_absen_pagi,
            potongan_lupa_absen_sore: Data.employee_profile.potongan_lupa_absen_sore,
            wajib_pajak: Data.employee_profile.wajib_pajak,
            tunjangan: Data.employee_profile.tunjangan,
            uang_makan: Data.employee_profile.uang_makan,
            gaji_bruto: Data.employee_profile.gaji_bruto,
            tunjangan_pajak: Data.employee_profile.tunjangan_pajak,
            hak_keuangan: Data.employee_profile.hak_keuangan,
            hak_askom: Data.employee_profile.hak_askom,
        })
    }

    render() {
        const {
            approval_pertama, approval_kedua, potongan_terlambat, potongan_pulang_awal,
            potongan_lupa_absen_pagi, potongan_lupa_absen_sore, wajib_pajak, tunjangan, uang_makan,
            gaji_bruto, tunjangan_pajak, hak_keuangan, hak_askom
        } = this.state
        return (
            <View style={{
                flex: 1,
                paddingHorizontal: wp(7)
                // backgroundColor: constant.COLOR.BACKGROUND
            }}>
                <View style={{ flexDirection: 'row' }}>
                    <Text style={[Font.text10tittle, styles.tittleLeft]}>Approval Pertama</Text>
                    <Text
                        style={[Font.text10Bold, styles.tittleRight]}
                    >{`: ${approval_pertama}`}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                    <Text style={[Font.text10tittle, styles.tittleLeft]}>Approval Kedua</Text>
                    <Text
                        style={[Font.text10Bold, styles.tittleRight]}
                    >{`: ${approval_kedua}`}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                    <Text style={[Font.text10tittle, styles.tittleLeft]}>Potongan Terlambat</Text>
                    <Text
                        style={[Font.text10Bold, styles.tittleRight]}
                    >{`: ${potongan_terlambat}`}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                    <Text style={[Font.text10tittle, styles.tittleLeft]}>Potongan Pulang Awal</Text>
                    <Text
                        style={[Font.text10Bold, styles.tittleRight]}
                    >{`: ${potongan_pulang_awal}`}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                    <Text style={[Font.text10tittle, styles.tittleLeft]}>Potongan Lupa Absen Pagi</Text>
                    <Text
                        style={[Font.text10Bold, styles.tittleRight]}
                    >{`: ${potongan_lupa_absen_pagi}`}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                    <Text style={[Font.text10tittle, styles.tittleLeft]}>Potongan Lupa Absen Sore</Text>
                    <Text
                        style={[Font.text10Bold, styles.tittleRight]}
                    >{`: ${potongan_lupa_absen_sore}`}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                    <Text style={[Font.text10tittle, styles.tittleLeft]}>Wajib Pajak </Text>
                    <Text
                        style={[Font.text10Bold, styles.tittleRight]}
                    >{`: ${wajib_pajak}`}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                    <Text style={[Font.text10tittle, styles.tittleLeft]}>Tunjangan</Text>
                    <Text
                        style={[Font.text10Bold, styles.tittleRight]}
                    >{`: ${tunjangan}`}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                    <Text style={[Font.text10tittle, styles.tittleLeft]}>Uang Makan </Text>
                    <Text
                        style={[Font.text10Bold, styles.tittleRight]}
                    >{`: ${uang_makan}`}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                    <Text style={[Font.text10tittle, styles.tittleLeft]}>Gaji Bruto </Text>
                    <Text
                        style={[Font.text10Bold, styles.tittleRight]}
                    >{`: ${gaji_bruto}`}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                    <Text style={[Font.text10tittle, styles.tittleLeft]}>Tunjangan Pajak </Text>
                    <Text
                        style={[Font.text10Bold, styles.tittleRight]}
                    >{`: ${tunjangan_pajak}`}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                    <Text style={[Font.text10tittle, styles.tittleLeft]}>Hak Keuangan</Text>
                    <Text
                        style={[Font.text10Bold, styles.tittleRight]}
                    >{`: ${hak_keuangan}`}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                    <Text style={[Font.text10tittle, styles.tittleLeft]}>Hak Askom</Text>
                    <Text
                        style={[Font.text10Bold, styles.tittleRight]}
                    >{`: ${hak_askom}`}</Text>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    tittleLeft: {
        width: wp(35),
        marginTop: hp(1.5),
        color: Color.COLOR.GREYBUTTON
    },
    tittleRight: {
        width: wp(54),
        borderBottomWidth: 1,
        color: Color.COLOR.BLUE,
        marginTop: hp(1.5)
    }
});

export default AbsenTukin;
