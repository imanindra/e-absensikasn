import React, { Component } from 'react';
import {
    View,
    Text,
    Image,
    StyleSheet,
    TextInput,
    Platform
} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import CardView from 'react-native-cardview';
import ButtonText from '../../../components/ButtonDisable';
import ButtonDisable from '../../../components/ButtonDisable';
import { Actions } from 'react-native-router-flux';
import Storage from '../../../data/Storage';
import { changePassword } from '../../../services/api';
import Font from '../../../constant/Font';
import Color from '../../../constant/Color'


class Password extends Component {
    constructor(props) {
        super(props);
        this.state = {
            new_password: '',
            confirm_password: ''
        };
    }

    async componentDidMount() {
        const Data = await Storage.getUser()
        // console.log('LLLL', Data)
        this.setState({
            ClientID: Data.client_id,
            employee_id: Data.employee_id
        })
    }

    async onChangePassword() {
        const { ClientID, employee_id, new_password, confirm_password } = this.state
        if (this.formValidation()) {
            await changePassword(ClientID, employee_id, new_password, confirm_password).then(data => {
                console.log('== CHANGE PASSWORD ==', data)
                if (data.code === "0") {
                    alert('Password Berhasil Diganti')
                } else (
                    alert('Gagal Mengganti password')
                )
            })
        }
    }

    formValidation() {
        const { new_password, confirm_password } = this.state
        if (new_password === '') {
            alert('New Password tidak boleh kosong')
            return false;
        }
        if (confirm_password === '') {
            alert('Confirm passoword tidak boleh kosong')
            return false;
        }
        if (new_password != confirm_password) {
            alert('New Password dan Konfirmasi Password tidak sama')
            return false;
        }
        return true
    }


    render() {
        return (
            <View style={{
                flex: 1,
                paddingHorizontal: wp(5)
                // backgroundColor: constant.COLOR.BACKGROUND
            }}>
                <View style={{ marginVertical: hp(2) }}>
                    <Text style={[Font.text12, { width: wp(30), textAlignVertical: 'center', marginVertical: hp(1), color: Color.COLOR.BLUE }]}>Password Baru</Text>
                    <CardView
                        cardElevation={2}
                        cardMaxElevation={2}
                        cornerRadius={5}>
                        <TextInput
                            // placeholder={'Password Baru'}
                            autoCapitalize='characters'
                            style={[Font.text12, { width: wp(99.231), height: hp(5) }]}
                            onChangeText={(text) => this.setState({ new_password: text })}
                            autoCapitalize={'characters'}
                        />
                    </CardView>
                </View>

                <View style={{ justifyContent: 'space-between' }}>
                    <Text style={[Font.text12, { textAlignVertical: 'center', marginVertical: hp(1), color: Color.COLOR.BLUE }]}>Konfirmasi Password Baru</Text>
                    <CardView
                        cardElevation={2}
                        cardMaxElevation={2}
                        cornerRadius={5}>
                        <TextInput
                            // placeholder={'Konfirmasi Password Baru'}
                            autoCapitalize='characters'
                            style={[Font.text12, { width: wp(99.231), height: hp(5) }]}
                            onChangeText={(text) => this.setState({ confirm_password: text })}
                            autoCapitalize={'characters'}
                        />
                    </CardView>
                </View>

                <View style={{ justifyContent: 'space-around', width: wp(70), alignSelf: 'center' }}>
                    {/* <View
                        style={{ width: wp(25) }}
                    /> */}

                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                        <View style={{ alignSelf: 'center', width: wp(33.5), paddingHorizontal: wp(5) }}>
                            <ButtonText
                                pressButton={() => this.onChangePassword()}
                                textButton={'GANTI'}
                                stylesnya={[Font.text12Bold, { color: 'white' }]}
                            />
                        </View>

                        <View style={{ alignSelf: 'center', width: wp(33.5), paddingHorizontal: wp(5) }}>
                            <ButtonDisable
                                status
                                pressButton={() => Actions.pop()}
                                textButton={'BATAL'}
                                stylesnya={[Font.text12Bold, { color: 'white' }]}
                            />
                        </View>
                    </View>
                </View>

            </View>
        )
    }
}

const styles = StyleSheet.create({
    logo: {
        resizeMode: 'contain',
        width: wp(55),
        height: hp(30)
    },
});

export default Password;
