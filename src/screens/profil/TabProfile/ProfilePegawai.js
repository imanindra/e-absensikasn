import React, { Component } from 'react';
import {
    View,
    Text,
    Image,
    StyleSheet,
    TextInput,
    FlatList
} from 'react-native';
import { color } from 'react-native-reanimated';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Color from '../../../constant/Color';
import Font from '../../../constant/Font';
import Storage from '../../../data/Storage';

class ProfilePegawai extends Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }

    async componentDidMount() {
        const Data = await Storage.getUser()
        this.setState({
            absensi_id: Data.employee_profile.absensi_id,
            nip: Data.employee_profile.nip,
            nama: Data.employee_profile.nama,
            jenis_kelamin: Data.employee_profile.jenis_kelamin,
            status_perkawinan: Data.employee_profile.status_perkawinan,
            jumlah_tanggungan_keluarga: Data.employee_profile.jumlah_tanggungan_keluarga,
            npwp: Data.employee_profile.npwp,
            rekening_bank: Data.employee_profile.rekening_bank,
            email_kantor: Data.employee_profile.email_kantor,
            status: Data.employee_profile.status_karyawan,
            tanggal_lahir: Data.employee_profile.tanggal_lahir,
            tanggal_bergabung: Data.employee_profile.tanggal_bergabung,
        })
    }

    render() {
        const { absensi_id, nip, nama, jenis_kelamin, status_perkawinan, jumlah_tanggungan_keluarga,
            npwp, rekening_bank, email_kantor, status, tanggal_lahir, tanggal_bergabung
        } = this.state
        return (
            <View style={{
                flex: 1,
                paddingHorizontal: wp(7)
                // backgroundColor: constant.COLOR.BACKGROUND
            }}>
                <View style={{ flexDirection: 'row' }}>
                    <Text style={[Font.text10tittle, styles.tittleLeft]}>Absen ID </Text>
                    <Text
                        style={[Font.text10Bold, styles.tittleRight]}
                    >{`: ${absensi_id}`}</Text>
                </View>

                <View style={{ flexDirection: 'row' }}>
                    <Text style={[Font.text10tittle, styles.tittleLeft]}>NIP </Text>
                    <Text
                        style={[Font.text10Bold, styles.tittleRight]}
                    >{`: ${nip}`}</Text>
                </View>

                <View style={{ flexDirection: 'row' }}>
                    <Text style={[Font.text10tittle, styles.tittleLeft]}>Nama Pegawai </Text>
                    <Text
                        style={[Font.text10Bold, styles.tittleRight]}
                    >{`: ${nama}`}</Text>
                </View>

                <View style={{ flexDirection: 'row' }}>
                    <Text style={[Font.text10tittle, styles.tittleLeft]}>Jenis Kelamin </Text>
                    <Text
                        style={[Font.text10Bold, styles.tittleRight]}
                    >{`: ${jenis_kelamin}`}</Text>
                </View>

                <View style={{ flexDirection: 'row' }}>
                    <Text style={[Font.text10tittle, styles.tittleLeft]}>Status Perkawinan </Text>
                    <Text
                        style={[Font.text10Bold, styles.tittleRight]}
                    >{`: ${status_perkawinan}`}</Text>
                </View>

                <View style={{ flexDirection: 'row' }}>
                    <Text style={[Font.text10tittle, styles.tittleLeft]}>Jumlah Tanggungan Keluarga </Text>
                    <Text
                        style={[Font.text10Bold, styles.tittleRight]}
                    >{`: ${jumlah_tanggungan_keluarga}`}</Text>
                </View>

                <View style={{ flexDirection: 'row' }}>
                    <Text style={[Font.text10tittle, styles.tittleLeft]}>NPWP</Text>
                    <Text
                        style={[Font.text10Bold, styles.tittleRight]}
                    >{`: ${npwp}`}</Text>
                </View>

                <View style={{ flexDirection: 'row' }}>
                    <Text style={[Font.text10tittle, styles.tittleLeft]}>Rekening Bank</Text>
                    <Text
                        style={[Font.text10Bold, styles.tittleRight]}
                    >{`: ${rekening_bank}`}</Text>
                </View>

                <View style={{ flexDirection: 'row' }}>
                    <Text style={[Font.text10tittle, styles.tittleLeft]}>Email Kantor</Text>
                    <Text
                        style={[Font.text10Bold, styles.tittleRight]}
                    >{`: ${email_kantor}`}</Text>
                </View>

                <View style={{ flexDirection: 'row' }}>
                    <Text style={[Font.text10tittle, styles.tittleLeft]}>Status</Text>
                    <Text
                        style={[Font.text10Bold, styles.tittleRight]}
                    >{`: ${status}`}</Text>
                </View>

                <View style={{ flexDirection: 'row' }}>
                    <Text style={[Font.text10tittle, styles.tittleLeft]}>Tanggal Lahir</Text>
                    <Text
                        style={[Font.text10Bold, styles.tittleRight]}
                    >{`: ${tanggal_lahir}`}</Text>
                </View>

                <View style={{ flexDirection: 'row' }}>
                    <Text style={[Font.text10tittle, styles.tittleLeft]}>Tanggal Bergabung</Text>
                    <Text
                        style={[Font.text10Bold, styles.tittleRight]}
                    >{`: ${tanggal_bergabung}`}</Text>
                </View>






            </View>
        )
    }
}

const styles = StyleSheet.create({
    tittleLeft: {
        width: wp(40),
        marginTop: hp(1.5),
        color : Color.COLOR.GREYBUTTON
    },
    tittleRight: {
        width: wp(49),
        borderBottomWidth: 1,
        color: Color.COLOR.BLUE,
        marginTop: hp(1.5)
    },
});

export default ProfilePegawai;
