import React, { Component } from 'react';
import {
    View,
    Text,
    Image,
    StyleSheet,
    TextInput
} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Color from '../../../constant/Color';
import Font from '../../../constant/Font';
import Storage from '../../../data/Storage';

class UnitKerja extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    async componentDidMount() {
        const Data = await Storage.getUser()
        this.setState({
            status_pegawai: Data.employee_profile.status_pegawai,
            golongan: Data.employee_profile.golongan,
            jabatan: Data.employee_profile.jabatan,
            eselon: Data.employee_profile.eselon,
            kelas_jabatan: Data.employee_profile.kelas_jabatan,
            unit_kerja: Data.employee_profile.unit_kerja,
        })
    }

    render() {
        const { status_pegawai, golongan, jabatan, eselon, kelas_jabatan, unit_kerja } = this.state
        return (
            <View style={{
                flex: 1,
                paddingHorizontal: wp(7)
                // backgroundColor: constant.COLOR.BACKGROUND
            }}>
                <View style={{ flexDirection: 'row' }}>
                    <Text style={[Font.text10tittle, styles.tittleLeft]}>Status Pegawai</Text>
                    <Text
                        style={[Font.text10Bold, styles.tittleRight]}
                    >{`: ${status_pegawai}`}</Text>
                </View>

                <View style={{ flexDirection: 'row' }}>
                    <Text style={[Font.text10tittle, styles.tittleLeft]}>Golongan</Text>
                    <Text
                        style={[Font.text10Bold, styles.tittleRight]}
                    >{`: ${golongan}`}</Text>
                </View>

                <View style={{ flexDirection: 'row' }}>
                    <Text style={[Font.text10tittle, styles.tittleLeft]}>Jabatan</Text>
                    <Text
                        style={[Font.text10Bold, styles.tittleRight]}
                    >{`: ${jabatan}`}</Text>
                </View>

                <View style={{ flexDirection: 'row' }}>
                    <Text style={[Font.text10tittle, styles.tittleLeft]}>Eselon</Text>
                    <Text
                        style={[Font.text10Bold, styles.tittleRight]}
                    >{`: ${eselon}`}</Text>
                </View>

                <View style={{ flexDirection: 'row' }}>
                    <Text style={[Font.text10tittle, styles.tittleLeft]}>Kelas Jabatan</Text>
                    <Text
                        style={[Font.text10Bold, styles.tittleRight]}
                    >{`: ${kelas_jabatan}`}</Text>
                </View>

                <View style={{ flexDirection: 'row' }}>
                    <Text style={[Font.text10tittle, styles.tittleLeft]}>Unit Kerja</Text>
                    <Text
                        style={[Font.text10Bold, styles.tittleRight]}
                    >{`: ${unit_kerja}`}</Text>
                </View>

            </View>
        )
    }
}

const styles = StyleSheet.create({
    tittleLeft: {
        width: wp(35),
        marginTop: hp(1.5),
        color: Color.COLOR.GREYBUTTON
    },
    tittleRight: {
        width: wp(54),
        borderBottomWidth: 1,
        color: Color.COLOR.BLUE,
        marginTop: hp(1.5)
    }
})

export default UnitKerja;
