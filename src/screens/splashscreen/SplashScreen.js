import React, { Component } from 'react';
import {
    View,
    Text,
    Image,
    StyleSheet,
    TextInput,
    StatusBar,
    AppRegistry
} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import assets from '../../assets';
import constant from '../../constant/Color';
import { Actions } from 'react-native-router-flux';
import Storage from '../../data/Storage'
import codePush from "react-native-code-push";

class SplashScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    async componentDidMount() {
        StatusBar.setHidden(true);
        setTimeout(function () {
            // Actions.Login();
        }, 5000)
        await Storage.getUser().then(token => {
            if (token) {
                this.setState({
                    token: token.client_id,
                    isInitiated: true,
                });
                setTimeout(function () {
                    Actions.Home();
                }, 3000)
            } else {
                this.setState({ isInitiated: true });
                setTimeout(function () {
                    Actions.Login();
                }, 3000)
            }
        });
    }

    componentWillUnmount() {
        StatusBar.setHidden(false);
    }

    render() {
        return (
            <View style={{
                flex: 1,
                backgroundColor: constant.COLOR.YELLOW,
                justifyContent: 'center'
            }}>
                <Image
                    source={assets.icon_logo}
                    style={{
                        resizeMode: 'center',
                        width: wp(100),
                        height: hp(50),
                    }}
                />
            </View >
        )
    }
}

const styles = StyleSheet.create({
    logo: {
        resizeMode: 'contain',
        width: wp(55),
        height: hp(30)
    },
});

// let splashScreenOptions = { checkFrequency: codePush.CheckFrequency.ON_APP_START, installMode: codePush.InstallMode.IMMEDIATE };
// SplashScreen = codePush(splashScreenOptions)(SplashScreen);
export default SplashScreen;

// AppRegistry.registerComponent('SplashScreen', () => SplashScreen);

