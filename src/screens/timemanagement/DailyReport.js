import React, { Component } from 'react';
import {
    View,
    Text,
    Image,
    StyleSheet,
    TextInput,
    FlatList
} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Color from '../../constant/Color';
import Font from '../../constant/Font';
import Storage from '../../data/Storage';
import { dailyReport } from '../../services/api';
import HeaderBackTitle from '../../components/HeaderBackTitle';
import { Actions } from 'react-native-router-flux';
import DropDownPicker from 'react-native-dropdown-picker';
import CardView from 'react-native-cardview';
import moment from "moment";
import { constant } from 'lodash';

class DailyReport extends Component {
    constructor(props) {
        super(props);
        this.state = {
            bulan: moment().format('MM'),
            tahun: moment().format('YYYY'),
            dailydata: []
        };
    }

    async componentDidMount() {
        const Data = await Storage.getUser();
        this.setState({
            employee_id: Data.employee_id,
            absensi_id: Data.absensi_id,
            ClientID: Data.client_id,
        })
        this.onDailyReport()
    }

    async onDailyReport() {
        const { employee_id, absensi_id, ClientID, bulan, tahun } = this.state
        await dailyReport(ClientID, absensi_id, employee_id, bulan, tahun).then(ok => {
            console.log('== GET DAILY REPORT ==', ok)
            this.setState({
                dailydata: ok.daily_report
            })
        })
    }

    render() {
        const {
            tanggal, jam_kerja, scan_masuk, scan_pulang, terlambat, pulang_awal, jumlah_jam_kerja,
            keterangan, dailydata
        } = this.state
        return (
            <View style={{
                flex: 1,
                backgroundColor: Color.COLOR.YELLOW
            }}>
                <HeaderBackTitle
                    BackButton
                    onPress={() => Actions.pop()}
                    titleText={'DAILY REPORT'}
                />
                <View style={{
                    marginTop: hp(6),


                }}>
                    <View style={{
                        paddingHorizontal: wp(5),
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        // marginTop: hp(2)
                    }}>
                        <View style={{ alignItems: 'center' }}>
                            <Text style={[Font.text12, { color: Color.COLOR.WHITE }]}>Bulan</Text>
                            <DropDownPicker
                                items={[
                                    { label: 'JANUARI', value: '01' },
                                    { label: 'FEBRUARI', value: '02' },
                                    { label: 'MARET', value: '03' },
                                    { label: 'APRIL', value: '04' },
                                    { label: 'MEI', value: '05' },
                                    { label: 'JUNI', value: '06' },
                                    { label: 'JULI', value: '07' },
                                    { label: 'AGUSTUS', value: '08' },
                                    { label: 'SEPTEMBER', value: '09' },
                                    { label: 'OKTOBER', value: '10' },
                                    { label: 'NOVEMBER', value: '11' },
                                    { label: 'DESEMBER', value: '12' },
                                ]}
                                labelStyle={[Font.text12Bold, { color: Color.COLOR.BLUE }]}
                                defaultValue={this.state.bulan}
                                style={[styles.shadowView, {
                                    borderTopLeftRadius: wp(10),
                                    borderTopRightRadius: wp(10),
                                    borderBottomLeftRadius: wp(10),
                                    borderBottomRightRadius: wp(10),
                                    backgroundColor: Color.COLOR.SOFTYELLOW, color: Color.COLOR.BLUE
                                }]}
                                dropDownStyle={[styles.shadowView, { backgroundColor: Color.COLOR.SOFTYELLOW }]}
                                containerStyle={{ height: 40, width: wp(43) }}
                                onChangeItem={item => this.setState({ bulan: item.value }, () => this.onDailyReport())}
                            />
                        </View>

                        <View style={{ alignItems: 'center' }}>
                            <Text style={{ color: Color.COLOR.WHITE }}>Tahun</Text>
                            <DropDownPicker
                                items={[
                                    { label: '2018', value: '2018' },
                                    { label: '2019', value: '2019' },
                                    { label: '2020', value: '2020' },
                                    { label: '2021', value: '2021' },
                                    { label: '2022', value: '2022' },
                                    { label: '2023', value: '2023' },
                                    { label: '2024', value: '2024' },
                                    { label: '2025', value: '2025' },
                                ]}
                                labelStyle={[Font.text12Bold, { color: Color.COLOR.BLUE }]}
                                defaultValue={this.state.tahun}
                                style={[styles.shadowView, {
                                    borderTopLeftRadius: wp(10),
                                    borderTopRightRadius: wp(10),
                                    borderBottomLeftRadius: wp(10),
                                    borderBottomRightRadius: wp(10),
                                    backgroundColor: Color.COLOR.SOFTYELLOW,
                                    color: Color.COLOR.BLUE
                                }]}
                                dropDownStyle={[styles.shadowView, { backgroundColor: Color.COLOR.SOFTYELLOW }]}
                                containerStyle={{ height: 40, width: wp(43) }}
                                onChangeItem={item => this.setState({ tahun: item.value }, () => this.onDailyReport())}
                            />
                        </View>
                    </View>

                    <View style={[styles.shadowView, {
                        height: hp(100),
                        paddingVertical: hp(1),
                        marginTop: hp(1),
                        borderTopRightRadius: wp(7),
                        borderTopLeftRadius: wp(7),
                        backgroundColor: Color.COLOR.SOFTYELLOW,
                        // height: hp(76),
                        borderRadius: wp(2)
                    }]}>


                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', borderBottomWidth: 1, marginTop: hp(1) }}>
                            <View style={{ width: wp(16, 67), alignSelf: 'center' }}>
                                <Text style={[Font.text8BoldTittles, { color: Color.COLOR.BLUE, textAlign: 'center' }]}>{'Tanggal'}</Text>
                            </View>
                            <View style={{ width: wp(16, 67), alignItems: 'center', borderLeftWidth: 1 }}>
                                <Text style={[Font.text8BoldTittles, { color: Color.COLOR.BLUE, textAlign: 'center' }]}>{'Scan Masuk'}</Text>
                            </View>
                            <View style={{ width: wp(16, 67), alignItems: 'center', borderLeftWidth: 1 }}>
                                <Text style={[Font.text8BoldTittles, { color: Color.COLOR.BLUE, textAlign: 'center' }]}>{'Scan Pulang'}</Text>
                            </View>
                            <View style={{ width: wp(14, 67), alignItems: 'center', borderLeftWidth: 1 }}>
                                <Text style={[Font.text8BoldTittles, { color: Color.COLOR.BLUE, textAlign: 'center' }]}>{'Terlambat'}</Text>
                            </View>
                            <View style={{ width: wp(14, 67), alignItems: 'center', borderLeftWidth: 1 }}>
                                <Text style={[Font.text8BoldTittles, { color: Color.COLOR.BLUE, textAlign: 'center' }]}>{'Pulang Awal'}</Text>
                            </View>
                            <View style={{ width: wp(20, 67), alignItems: 'center', borderLeftWidth: 1 }}>
                                <Text style={[Font.text8BoldTittles, { color: Color.COLOR.BLUE, textAlign: 'center' }]}>{'Keterangan'}</Text>
                            </View>
                        </View>
                        <FlatList
                            data={dailydata}
                            renderItem={({ item }) =>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginVertical: hp(0.455) }}>
                                    <View style={{ width: wp(16.67), alignItems: 'center' }}>
                                        <Text style={[Font.text8Bolds, { color: Color.COLOR.BLUE }]}>{item.tanggal}</Text>
                                    </View>
                                    <View style={{ width: wp(16.67), alignItems: 'center' }}>
                                        <Text style={[Font.text8Bolds, { color: Color.COLOR.BLUE }]}>{item.scan_masuk}</Text>
                                    </View>
                                    <View style={{ width: wp(16.67), alignItems: 'center' }}>
                                        <Text style={[Font.text8Bolds, { color: Color.COLOR.BLUE }]}>{item.scan_pulang}</Text>
                                    </View>
                                    <View style={{ width: wp(14.67), alignItems: 'center' }}>
                                        <Text style={[Font.text8Bolds, { color: Color.COLOR.BLUE }]}>{item.terlambat}</Text>
                                    </View>
                                    <View style={{ width: wp(14.67), alignItems: 'center' }}>
                                        <Text style={[Font.text8Bolds, { color: Color.COLOR.BLUE }]}>{item.pulang_awal}</Text>
                                    </View>
                                    <View style={{ width: wp(20.67), alignItems: 'center' }}>
                                        <Text style={[Font.text8Bolds, { color: Color.COLOR.BLUE }]}>{item.keterangan}</Text>
                                    </View>
                                </View>
                            }
                        />
                    </View>
                </View>
            </View >
        )
    }
}

const styles = StyleSheet.create({
    // logo: {
    //     resizeMode: 'contain',
    //     width: wp(55),
    //     height: hp(30)
    // },
    // tittleLeft: {
    //     width: wp(43),
    //     color: '#AAA7A7',
    //     marginTop: wp(1)
    // },
    // tittleRight: {
    //     width: wp(46),
    //     borderBottomWidth: 1,
    //     color: Color.COLOR.BLUE,
    //     marginTop: wp(1)
    // },
    shadowView: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
    },
});

export default DailyReport;
