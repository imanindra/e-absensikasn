import React, { Component } from 'react';
import {
    View,
    Text,
    Image,
    StyleSheet,
    TextInput,
    FlatList,
    ScrollView
} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Color from '../../constant/Color';
import Font from '../../constant/Font';
import Storage from '../../data/Storage';
import { dailyReport, monthReport } from '../../services/api';
import HeaderBackTitle from '../../components/HeaderBackTitle';
import { Actions } from 'react-native-router-flux';
import DropDownPicker from 'react-native-dropdown-picker';
import CardView from 'react-native-cardview';
import moment from "moment";

class MonthReport extends Component {
    constructor(props) {
        super(props);
        this.state = {
            bulan: moment().format('MM'),
            tahun: moment().format('YYYY'),
            dailydata: []
        };
    }

    async componentDidMount() {
        const Data = await Storage.getUser();
        this.setState({
            employee_id: Data.employee_id,
            absensi_id: Data.absensi_id,
            ClientID: Data.client_id,
        })
        this.onMonthReport()
    }

    async onMonthReport() {
        const { employee_id, absensi_id, ClientID, bulan, tahun } = this.state
        await monthReport(ClientID, absensi_id, employee_id, bulan, tahun).then(ok => {
            console.log('== GET Month REPORT ==', ok)
            this.setState({
                jumlah_hari: ok.daily_report.jumlah_hari,
                hari_kerja: ok.daily_report.hari_kerja,
                hadir: ok.daily_report.hadir,
                st: ok.daily_report.st,
                sakit: ok.daily_report.sakit,
                izin: ok.daily_report.izin,
                cuti: ok.daily_report.cuti,
                tidak_absen_pagi: ok.daily_report.tidak_absen_pagi,
                tidak_absen_sore: ok.daily_report.tidak_absen_sore,
                total_hadir_hari_kerja: ok.daily_report.total_hadir_hari_kerja,
                tidak_hadir: ok.daily_report.tidak_hadir,
                hadir_hari_libur: ok.daily_report.hadir_hari_libur,
                total_hadir: ok.daily_report.total_hadir,
                terlambat: ok.daily_report.terlambat,
                pulang_cepat: ok.daily_report.pulang_cepat
            })
        })
    }

    render() {
        const {
            jumlah_hari, hari_kerja, hadir, st, sakit, cuti, izin,
            tidak_absen_pagi, tidak_absen_sore, total_hadir_hari_kerja,
            tidak_hadir, hadir_hari_libur,
            total_hadir, terlambat, pulang_cepat
        } = this.state
        return (
            <View style={{
                flex: 1,
                backgroundColor: Color.COLOR.YELLOW
            }}>
                <HeaderBackTitle
                    BackButton
                    onPress={() => Actions.pop()}
                    titleText={'MONTH REPORT'}
                />

                <View style={{
                    marginTop: hp(6),
                    paddingHorizontal: wp(5),
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    marginBottom: hp(2)
                }}>
                    <View style={{ alignItems: 'center' }}>
                        <Text style={[Font.text12, { color: Color.COLOR.WHITE }]}>Bulan</Text>
                        <DropDownPicker
                            items={[
                                { label: 'JANUARI', value: '01' },
                                { label: 'FEBRUARI', value: '02' },
                                { label: 'MARET', value: '03' },
                                { label: 'APRIL', value: '04' },
                                { label: 'MEI', value: '05' },
                                { label: 'JUNI', value: '06' },
                                { label: 'JULI', value: '07' },
                                { label: 'AGUSTUS', value: '08' },
                                { label: 'SEPTEMBER', value: '09' },
                                { label: 'OKTOBER', value: '10' },
                                { label: 'NOVEMBER', value: '11' },
                                { label: 'DESEMBER', value: '12' },
                            ]}
                            labelStyle={[Font.text12Bold, { color: Color.COLOR.BLUE }]}
                            defaultValue={this.state.bulan}
                            style={[styles.shadowView, {
                                borderTopLeftRadius: wp(10),
                                borderTopRightRadius: wp(10),
                                borderBottomLeftRadius: wp(10),
                                borderBottomRightRadius: wp(10),
                                backgroundColor: Color.COLOR.SOFTYELLOW, color: Color.COLOR.BLUE
                            }]}
                            dropDownStyle={[styles.shadowView, { backgroundColor: Color.COLOR.SOFTYELLOW }]}
                            containerStyle={{ height: 40, width: wp(43) }}
                            onChangeItem={item => this.setState({ bulan: item.value }, () => this.onMonthReport())}
                        />
                    </View>

                    <View style={{ alignItems: 'center' }}>
                        <Text style={[Font.text12, { color: Color.COLOR.WHITE }]}>Tahun</Text>
                        <DropDownPicker
                            items={[
                                { label: '2018', value: '2018' },
                                { label: '2019', value: '2019' },
                                { label: '2020', value: '2020' },
                                { label: '2021', value: '2021' },
                                { label: '2022', value: '2022' },
                                { label: '2023', value: '2023' },
                                { label: '2024', value: '2024' },
                                { label: '2025', value: '2025' },
                            ]}
                            // style={Font.text12}
                            labelStyle={[Font.text12Bold, { color: Color.COLOR.BLUE }]}
                            style={[styles.shadowView, {
                                borderTopLeftRadius: wp(10),
                                borderTopRightRadius: wp(10),
                                borderBottomLeftRadius: wp(10),
                                borderBottomRightRadius: wp(10),
                                backgroundColor: Color.COLOR.SOFTYELLOW, color: Color.COLOR.BLUE
                            }]}
                            dropDownStyle={[styles.shadowView, { backgroundColor: Color.COLOR.SOFTYELLOW }]}
                            defaultValue={this.state.tahun}
                            containerStyle={{ height: 40, width: wp(43) }}
                            onChangeItem={item => this.setState({ tahun: item.value }, () => this.onMonthReport())}
                        />
                    </View>
                </View>

                <View style={[styles.shadowView, {
                    backgroundColor: Color.COLOR.SOFTYELLOW,
                    borderTopRightRadius: wp(7),
                    borderTopLeftRadius: wp(7),

                }]}>

                    {/* List Laporan */}
                    <ScrollView
                        style={{
                            paddingHorizontal: wp(5),
                            zIndex: -99,
                            height: hp(100),

                        }}>
                        <View style={{ flexDirection: 'row' }}>
                            <Text style={[Font.text10tittle, styles.tittleLeft]}>Jumlah Hari</Text>
                            <Text
                                style={[Font.text10tittle, styles.tittleRight]}
                            >{`: ${jumlah_hari}`}</Text>
                        </View>
                        <View style={{ flexDirection: 'row' }}>
                            <Text style={[Font.text10tittle, styles.tittleLeft]}>Hari Kerja</Text>
                            <Text
                                style={[Font.text10tittle, styles.tittleRight]}
                            >{`: ${hari_kerja}`}</Text>
                        </View>
                        <View style={{ flexDirection: 'row' }}>
                            <Text style={[Font.text10tittle, styles.tittleLeft]}>Hadir</Text>
                            <Text
                                style={[Font.text10tittle, styles.tittleRight]}
                            >{`: ${hadir}`}</Text>
                        </View>
                        <View style={{ flexDirection: 'row' }}>
                            <Text style={[Font.text10tittle, styles.tittleLeft]}>ST</Text>
                            <Text
                                style={[Font.text10tittle, styles.tittleRight]}
                            >{`: ${st}`}</Text>
                        </View>
                        <View style={{ flexDirection: 'row' }}>
                            <Text style={[Font.text10tittle, styles.tittleLeft]}>Sakit</Text>
                            <Text
                                style={[Font.text10tittle, styles.tittleRight]}
                            >{`: ${sakit}`}</Text>
                        </View>
                        <View style={{ flexDirection: 'row' }}>
                            <Text style={[Font.text10tittle, styles.tittleLeft]}>Izin</Text>
                            <Text
                                style={[Font.text10tittle, styles.tittleRight]}
                            >{`: ${izin}`}</Text>
                        </View>
                        <View style={{ flexDirection: 'row' }}>
                            <Text style={[Font.text10tittle, styles.tittleLeft]}>Cuti</Text>
                            <Text
                                style={[Font.text10tittle, styles.tittleRight]}
                            >{`: ${cuti}`}</Text>
                        </View>
                        <View style={{ flexDirection: 'row' }}>
                            <Text style={[Font.text10tittle, styles.tittleLeft]}>Tidak Absen Pagi</Text>
                            <Text
                                style={[Font.text10tittle, styles.tittleRight]}
                            >{`: ${tidak_absen_pagi}`}</Text>
                        </View>
                        <View style={{ flexDirection: 'row' }}>
                            <Text style={[Font.text10tittle, styles.tittleLeft]}>Tidak Absen Sore</Text>
                            <Text
                                style={[Font.text10tittle, styles.tittleRight]}
                            >{`: ${tidak_absen_sore}`}</Text>
                        </View>
                        <View style={{ flexDirection: 'row' }}>
                            <Text style={[Font.text10tittle, styles.tittleLeft]}>Total Hadir Hari Kerja</Text>
                            <Text
                                style={[Font.text10tittle, styles.tittleRight]}
                            >{`: ${total_hadir_hari_kerja}`}</Text>
                        </View>
                        <View style={{ flexDirection: 'row' }}>
                            <Text style={[Font.text10tittle, styles.tittleLeft]}>Tidak Hadir</Text>
                            <Text
                                style={[Font.text10tittle, styles.tittleRight]}
                            >{`: ${tidak_hadir}`}</Text>
                        </View>
                        <View style={{ flexDirection: 'row' }}>
                            <Text style={[Font.text10tittle, styles.tittleLeft]}>Hadir Hari Libur</Text>
                            <Text
                                style={[Font.text10tittle, styles.tittleRight]}
                            >{`: ${hadir_hari_libur}`}</Text>
                        </View>
                        <View style={{ flexDirection: 'row' }}>
                            <Text style={[Font.text10tittle, styles.tittleLeft]}>Total Hadir</Text>
                            <Text
                                style={[Font.text10tittle, styles.tittleRight]}
                            >{`: ${total_hadir}`}</Text>
                        </View>
                        <View style={{ flexDirection: 'row' }}>
                            <Text style={[Font.text10tittle, styles.tittleLeft]}>Terlambat</Text>
                            <Text
                                style={[Font.text10tittle, styles.tittleRight]}
                            >{`: ${terlambat}`}</Text>
                        </View>
                        <View style={{ flexDirection: 'row' }}>
                            <Text style={[Font.text10tittle, styles.tittleLeft]}>Pulang Cepat</Text>
                            <Text
                                style={[Font.text10tittle, styles.tittleRight]}
                            >{`: ${pulang_cepat}`}</Text>
                        </View>
                    </ScrollView>

                </View>
            </View >
        )
    }
}

const styles = StyleSheet.create({
    logo: {
        resizeMode: 'contain',
        width: wp(55),
        height: hp(30)
    },
    tittleLeft: {
        width: wp(34),
        color: Color.COLOR.BLUE,
        marginTop: wp(2)
    },
    tittleRight: {
        width: wp(58),
        borderBottomWidth: 1,
        color: Color.COLOR.BLUE,
        marginTop: wp(2)
    },
    shadowView: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
    },
});

export default MonthReport;
