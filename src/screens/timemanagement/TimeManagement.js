import React, { Component } from 'react';
import {
    View,
    Text,
    Image,
    StyleSheet,
    TextInput,
    Platform
} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Color from '../../constant/Color';
import HeaderBackTitle from '../../components/HeaderBackTitle';
import { Actions } from 'react-native-router-flux';
import MenuTM from '../../components/MenuTM';
import assets from '../../assets';

class TimeManagement extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    async componentDidMount() {
    }

    render() {
        const {
        } = this.state
        return (
            <View style={{
                flex: 1,
                backgroundColor: Color.COLOR.BACKGROUND
            }}>
                <HeaderBackTitle
                    BackButton
                    onPress={() => Actions.pop()}
                    titleText={'Time Management'}
                />

                <View style={{
                    marginTop: hp(5),
                    flexDirection: 'row',
                    justifyContent: 'space-evenly'
                }}>
                    <MenuTM
                        onPress={() => Actions.DailyReport()}
                        imageMenu={assets.icon_day}
                        menuText={'DAILY REPORT'}
                    />
                    <MenuTM
                        onPress={() => Actions.MonthReport()}
                        imageMenu={assets.icon_month}
                        menuText={'MONTH REPORT'}
                    />
                </View>

            </View>
        )
    }
}

const styles = StyleSheet.create({
    logo: {
        resizeMode: 'contain',
        width: wp(55),
        height: hp(30)
    },
    tittleLeft: {
        width: wp(43),
        color: '#AAA7A7',
        marginTop: hp(3)
    },
    tittleRight: {
        width: wp(46),
        borderBottomWidth: 1,
        color: Color.COLOR.BLUE,
        marginTop: hp(3)
    }
});

export default TimeManagement;
