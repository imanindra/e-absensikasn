import { store } from "../configureStore";
import {
  defaultHeaders,
  buildResponse,
  buildResponseNew,
  buildHeaders,
  buildParam,
  BASE_URL,
  buildResponseLogin,
  buildHeadersImage,
  SITE_URL,
} from "./apiHelper";

// ----- LOGIN ----- //
export function login(user, password) {
  return fetch(`${SITE_URL}login`, {
    headers: defaultHeaders,
    method: "POST",
    body: JSON.stringify({
      user,
      password,
    }),
  })
    .then((response) => buildResponse(response, "Error Login"))
    .then((data) => data);
}

// ----- CHANGE PASSWORD ----- //
export function changePassword(
  ClientID,
  employee_id,
  new_password,
  confirm_password
) {
  return fetch(`${SITE_URL}change-password`, {
    headers: {
      "Content-Type": "application/json",
      ClientID: ClientID,
    },
    method: "POST",
    body: JSON.stringify({
      employee_id,
      new_password,
      confirm_password,
    }),
  })
    .then((response) => buildResponse(response, "Error Change Password"))
    .then((data) => data);
}

// ----- GET JAM KERJA ----- //
export function getJamKerja(ClientID, employee_id, absensi_id) {
  return fetch(`${BASE_URL}get-jamkerja/${employee_id}/${absensi_id}`, {
    headers: {
      "Content-Type": "application/json",
      ClientID: ClientID,
    },
    method: "GET",
  })
    .then((response) => buildResponse(response, "Error Get Jam Kerja"))
    .then((data) => data);
}

// ----- GET CLOCK IN ----- //
export function getClockIn(ClientID, employee_id, absensi_id) {
  return fetch(`${BASE_URL}get-clockin/${employee_id}/${absensi_id}`, {
    headers: {
      "Content-Type": "application/json",
      ClientID: ClientID,
    },
    method: "GET",
  })
    .then((response) => buildResponse(response, "Error Get Clock In"))
    .then((data) => data);
}

// ----- CLOCK IN ----- //
export function clockIn(
  ClientID,
  absensi_id,
  employee_id,
  scan_masuk,
  latitute,
  longitute,
  address,
  foto,
  note
) {
  return fetch(`${BASE_URL}clockin`, {
    headers: {
      "Content-Type": "application/json",
      ClientID: ClientID,
    },
    method: "POST",
    body: JSON.stringify({
      absensi_id,
      employee_id,
      scan_masuk,
      latitute,
      longitute,
      address,
      foto,
      note,
    }),
  })
    .then((response) => buildResponse(response, "Error Post Clock In"))
    .then((data) => data);
}

// ----- CLOCK OUT ----- //
export function clockOut(
  ClientID,
  absensi_id,
  employee_id,
  scan_pulang,
  latitute,
  longitute,
  address,
  foto,
  note
) {
  return fetch(`${BASE_URL}clockout`, {
    headers: {
      "Content-Type": "application/json",
      ClientID: ClientID,
    },
    method: "POST",
    body: JSON.stringify({
      absensi_id,
      employee_id,
      scan_pulang,
      latitute,
      longitute,
      address,
      foto,
      note,
    }),
  })
    .then((response) => buildResponse(response, "Error Post Clock Out"))
    .then((data) => data);
}

// ----- GET CLOCK Out ----- //
export function getClockOut(ClientID, employee_id, absensi_id) {
  return fetch(`${BASE_URL}get-clockout/${employee_id}/${absensi_id}`, {
    headers: {
      "Content-Type": "application/json",
      ClientID: ClientID,
    },
    method: "GET",
  })
    .then((response) => buildResponse(response, "Error Get Clock Out"))
    .then((data) => data);
}

// ----- LOG TODAY ----- //
export function getLogToday(ClientID, employee_id, absensi_id) {
  return fetch(`${BASE_URL}get-log-today/${employee_id}`, {
    headers: {
      "Content-Type": "application/json",
      ClientID: ClientID,
    },
    method: "GET",
  })
    .then((response) => buildResponse(response, "ERROR GET LOG TODAY"))
    .then((data) => data);
}

// ----- GET NAME LOKASI ----- //

export function getNameLokasi(latitude, longitute) {
  return fetch(
    `http://www.mapquestapi.com/geocoding/v1/reverse?key=SheCybXGGreGhyU6eaEXmQ7DwmZrZEyl&location=${latitude},${longitute}`,
    {
      headers: {
        "Content-Type": "application/json",
      },
      method: "GET",
    }
  )
    .then((response) => buildResponse(response, "ERROR GET NAME LOKASI"))
    .then((data) => data);
}

// ----- REPORT ----- //

export function dailyReport(ClientID, absensi_id, employee_id, bulan, tahun) {
  return fetch(`${SITE_URL}report/dailyreport`, {
    headers: {
      "Content-Type": "application/json",
      ClientID: ClientID,
    },
    method: "POST",
    body: JSON.stringify({
      absensi_id,
      employee_id,
      bulan,
      tahun,
    }),
  })
    .then((response) => buildResponse(response, "Error Post Clock Out"))
    .then((data) => data);
}

export function monthReport(ClientID, absensi_id, employee_id, bulan, tahun) {
  return fetch(`${SITE_URL}report/monthlyreport`, {
    headers: {
      "Content-Type": "application/json",
      ClientID: ClientID,
    },
    method: "POST",
    body: JSON.stringify({
      absensi_id,
      employee_id,
      bulan,
      tahun,
    }),
  })
    .then((response) => buildResponse(response, "Error Post Clock Out"))
    .then((data) => data);
}

// ----- LOGOUT ----- //
export function getLogout(ClientID, employee_id) {
  return fetch(`${SITE_URL}${employee_id}`, {
    headers: {
      "Content-Type": "application/json",
      ClientID: ClientID,
    },
    method: "GET",
  })
    .then((response) => buildResponse(response, "Error Logout"))
    .then((data) => data);
}

// ----- END OPTION ----- //
